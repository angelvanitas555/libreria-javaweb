package modelo;

public class AlumnosBean {

    //mapeo de tabla alumno
    private int id;
    private String nombre;
    private String carrera;
    private UsuariosBean cod_usuario;

    //metodo constructor
    public AlumnosBean(int id) {
        this.id = id;
    }

    public UsuariosBean getCod_usuario() {
        return cod_usuario;
    }

    public void setCod_usuario(UsuariosBean cod_usuario) {
        this.cod_usuario = cod_usuario;
    }
    

    //getters y setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
}
