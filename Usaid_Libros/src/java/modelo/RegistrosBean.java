package modelo;

public class RegistrosBean {

    //mapeo de tabla registro
    private int id;
    private int prestamo;
    private AlumnosBean alumno;
    private LibrosBean ISBN;
    private StockBean stock;
    private ProveedoresBean proveedor;

    //metodo constructor
    public RegistrosBean(int id) {
        this.id = id;
    }

    //metodos getters y setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrestamo() {
        return prestamo;
    }

    public void setPrestamo(int prestamo) {
        this.prestamo = prestamo;
    }

    public AlumnosBean getAlumno() {
        return alumno;
    }

    public void setAlumno(AlumnosBean alumno) {
        this.alumno = alumno;
    }

    public LibrosBean getISBN() {
        return ISBN;
    }

    public void setISBN(LibrosBean ISBN) {
        this.ISBN = ISBN;
    }

    public StockBean getStock() {
        return stock;
    }

    public void setStock(StockBean stock) {
        this.stock = stock;
    }

    public ProveedoresBean getProveedor() {
        return proveedor;
    }

    public void setProveedor(ProveedoresBean proveedor) {
        this.proveedor = proveedor;
    }
    
}
