package modelo;

import java.util.Date;

public class SolicitudBean {

    int id;
    AlumnosBean id_alumno;
    LibrosBean id_libro;
    Date f_h_solicitud;

    public SolicitudBean(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AlumnosBean getId_alumno() {
        return id_alumno;
    }

    public void setId_alumno(AlumnosBean id_alumno) {
        this.id_alumno = id_alumno;
    }

    public LibrosBean getId_libro() {
        return id_libro;
    }

    public void setId_libro(LibrosBean id_libro) {
        this.id_libro = id_libro;
    }

    public Date getF_h_solicitud() {
        return f_h_solicitud;
    }

    public void setF_h_solicitud(Date f_h_solicitud) {
        this.f_h_solicitud = f_h_solicitud;
    }
    
}
