package modelo;

import java.util.Date;

public class PrestamoBean {

    int id;
    AlumnosBean id_alumno;
    LibrosBean ISBN;
    StockBean id_stock;
    Date f_prestamo;
    Date f_limite;
    AdministradoresBean id_administrador;
    boolean accion;

    public PrestamoBean(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AlumnosBean getId_alumno() {
        return id_alumno;
    }

    public void setId_alumno(AlumnosBean id_alumno) {
        this.id_alumno = id_alumno;
    }

    public LibrosBean getISBN() {
        return ISBN;
    }

    public void setISBN(LibrosBean ISBN) {
        this.ISBN = ISBN;
    }

    public StockBean getId_stock() {
        return id_stock;
    }

    public void setId_stock(StockBean id_stock) {
        this.id_stock = id_stock;
    }

    public Date getF_prestamo() {
        return f_prestamo;
    }

    public void setF_prestamo(Date f_prestamo) {
        this.f_prestamo = f_prestamo;
    }

    public Date getF_limite() {
        return f_limite;
    }

    public void setF_limite(Date f_limite) {
        this.f_limite = f_limite;
    }

    public AdministradoresBean getId_administrador() {
        return id_administrador;
    }

    public void setId_administrador(AdministradoresBean id_administrador) {
        this.id_administrador = id_administrador;
    }

    public boolean isAccion() {
        return accion;
    }

    public void setAccion(boolean accion) {
        this.accion = accion;
    }
    
    

}