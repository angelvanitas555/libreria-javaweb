package modelo;

import java.util.Date;

public class EntregaBean {

    int id_entrega;
    AlumnosBean id_alumno;
    LibrosBean isbn_libro;
    PrestamoBean fecha_prestamo;
    PrestamoBean fecha_limite;
    Date fecha_entrega;
    RegistrosBean n_registro;
    boolean mora;
    int dias_mora;

    public EntregaBean(int id_entrega) {
        this.id_entrega = id_entrega;
    }

    public RegistrosBean getN_registro() {
        return n_registro;
    }

    public void setN_registro(RegistrosBean n_registro) {
        this.n_registro = n_registro;
    }
    

    public int getId_entrega() {
        return id_entrega;
    }

    public void setId_entrega(int id_entrega) {
        this.id_entrega = id_entrega;
    }

    public AlumnosBean getId_alumno() {
        return id_alumno;
    }

    public void setId_alumno(AlumnosBean id_alumno) {
        this.id_alumno = id_alumno;
    }

    public LibrosBean getIsbn_libro() {
        return isbn_libro;
    }

    public void setIsbn_libro(LibrosBean isbn_libro) {
        this.isbn_libro = isbn_libro;
    }

    public PrestamoBean getFecha_prestamo() {
        return fecha_prestamo;
    }

    public void setFecha_prestamo(PrestamoBean fecha_prestamo) {
        this.fecha_prestamo = fecha_prestamo;
    }

    public PrestamoBean getFecha_limite() {
        return fecha_limite;
    }

    public void setFecha_limite(PrestamoBean fecha_limite) {
        this.fecha_limite = fecha_limite;
    }

    public Date getFecha_entrega() {
        return fecha_entrega;
    }

    public void setFecha_entrega(Date fecha_entrega) {
        this.fecha_entrega = fecha_entrega;
    }

    public boolean isMora() {
        return mora;
    }

    public void setMora(boolean mora) {
        this.mora = mora;
    }

    public int getDias_mora() {
        return dias_mora;
    }

    public void setDias_mora(int dias_mora) {
        this.dias_mora = dias_mora;
    }
    
    
}
