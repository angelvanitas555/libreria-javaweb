
package modelo;

import java.util.Date;

public class LibrosBean {

    //mapeo de tabla libros
    private int ISBN;
    private String titulo;
    private Date f_publicacion;
    private int n_tomo;
    private int n_edicion;
    private String genero;
    private int n_pag;
    private String editorial;
    private AutoresBean autor;

    //metodo constructor
    public LibrosBean(int ISBN) {
        this.ISBN = ISBN;
    }

    //metodos gettes y setters
    public int getISBN() {
        return ISBN;
    }

    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getF_publicacion() {
        return f_publicacion;
    }

    public void setF_publicacion(Date f_publicacion) {
        this.f_publicacion = f_publicacion;
    }

    public int getN_tomo() {
        return n_tomo;
    }

    public void setN_tomo(int n_tomo) {
        this.n_tomo = n_tomo;
    }

    public int getN_edicion() {
        return n_edicion;
    }

    public void setN_edicion(int n_edicion) {
        this.n_edicion = n_edicion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getN_pag() {
        return n_pag;
    }

    public void setN_pag(int n_pag) {
        this.n_pag = n_pag;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public AutoresBean getAutor() {
        return autor;
    }

    public void setAutor(AutoresBean autor) {
        this.autor = autor;
    }

}
