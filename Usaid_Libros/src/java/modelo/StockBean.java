package modelo;

public class StockBean {

    //mapeo de tabla stock
    private int id;
    private LibrosBean ISBN;
    private int disponibles;
    private int stock;
    private boolean existencia;
    private int dias_limite;

    //metodo constructor
    public StockBean(int id) {
        this.id = id;
    }

    //metodos getters y setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LibrosBean getISBN() {
        return ISBN;
    }

    public void setISBN(LibrosBean ISBN) {
        this.ISBN = ISBN;
    }

    public int getDisponibles() {
        return disponibles;
    }

    public void setDisponibles(int disponibles) {
        this.disponibles = disponibles;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public boolean getExistencia() {
        return existencia;
    }

    public void setExistencia(boolean existencia) {
        this.existencia = existencia;
    }

    public int getDias_limite() {
        return dias_limite;
    }

    public void setDias_limite(int dias_limite) {
        this.dias_limite = dias_limite;
    }

    

}
