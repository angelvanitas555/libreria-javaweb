package modelo;

import java.util.Date;

public class AutoresBean {

    //mapeo de tabla autores
    private int id;
    private String nombre;
    private String nacionalidad;
    private Date f_nac;

    //metodo constructor
    public AutoresBean(int id) {
        this.id = id;
    }

    //metodos getters y setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Date getF_nac() {
        return f_nac;
    }

    public void setF_nac(Date f_nac) {
        this.f_nac = f_nac;
    }

}
