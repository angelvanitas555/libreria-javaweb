package controlador;

import dao.Conexion;
import dao.Loggin_admin;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.AdministradoresBean;
import modelo.UsuariosBean;

public class LogginServlet extends HttpServlet {

    Conexion conn = new Conexion();
    String msg = "", jsp = "";
    RequestDispatcher rd;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "loggin":
                loggin(request, response);
                break;
            case "Log_dos":
                Log_dos(request, response);
                break;

        }
    }

    protected void loggin(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nombre = request.getParameter("nombre");
        String pass_admin = request.getParameter("pass_admin");

        AdministradoresBean adbean = new AdministradoresBean(0);
        Loggin_admin admin = new Loggin_admin(conn);
        boolean res = admin.Autenticacion(nombre, pass_admin);
        // List<AdministradoresBean>lista = admin.Autenticacion(nombre, pass_admin);
        if (res) {
            msg = "Administrador Detectado";
            jsp = "index.jsp";
            rd = request.getRequestDispatcher(jsp);
            rd.forward(request, response);
        } else {
            jsp = "error_de_pagina.jsp";
            msg = "Ingrese un Administrador";
            rd = request.getRequestDispatcher(jsp);
            rd.forward(request, response);
        }
       
    }
//ESTE ES EL QUE OCUPA EL PROGRAMA
    protected void Log_dos(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nombre = request.getParameter("usuario");
        String pass = request.getParameter("pass");

        Loggin_admin admin = new Loggin_admin(conn);

        UsuariosBean ubean = admin.loggin(nombre, pass);
        jsp = "loggin.jsp";
        try {
            if (ubean.getId() > 0) {
                //true=administradores y false=alumnos
                if (ubean.getTipo()) {
                    jsp="index.jsp";
                    msg = "Bienvenido Administrador";
                    request.setAttribute("msg", msg);
                    rd = request.getRequestDispatcher(jsp);
                } else {
                    jsp = "usuario_comun.jsp";
                    msg = "Bienvenido Usuario";
                    request.setAttribute("msg", msg);
                    rd = request.getRequestDispatcher(jsp);
                }
            }
        } catch (Exception e) {
            jsp="error_de_pagina.jsp";
            msg="ERROR de usuario y password";
            request.setAttribute("msg", msg);
            rd = request.getRequestDispatcher(jsp);
        }

        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
