package controlador;

import dao.AutoresDao;
import dao.Conexion;
import dao.LibrosDao;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.AutoresBean;
import modelo.LibrosBean;
import modelo.ProveedoresBean;


public class LibrosServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "modificar":
                modificar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
                case "consultarIn":
                consultarIn(request, response);
                break;
        }
    }

    //glovales
    Conexion conn = new Conexion();
    LibrosDao ldao = new LibrosDao(conn);
    String msg = "";
    RequestDispatcher rd;
    SimpleDateFormat formato = new SimpleDateFormat("yy/MM/dd");

    //consultarIn
    protected void consultarIn(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AutoresDao adao=new AutoresDao(conn);
        List<AutoresBean> lista = adao.consultarAll();
        
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/linsertar.jsp");
        rd.forward(request, response);
    }
    
    //insertar
    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {

        String titulo = request.getParameter("titulo");
        Date f_publicacion = formato.parse(request.getParameter("f_publicacion"));
        int n_tomo = Integer.parseInt(request.getParameter("n_tomo"));
        int n_edicion = Integer.parseInt(request.getParameter("n_edicion"));
        String genero = request.getParameter("genero");
        int n_pag = Integer.parseInt(request.getParameter("n_pag"));
        String editorial = request.getParameter("editorial");
        int autor = Integer.parseInt(request.getParameter("autor"));

        LibrosBean lbean = new LibrosBean(0);
        AutoresBean abean = new AutoresBean(autor);
        lbean.setTitulo(titulo);
        lbean.setF_publicacion(f_publicacion);
        lbean.setN_tomo(n_tomo);
        lbean.setN_edicion(n_edicion);
        lbean.setGenero(genero);
        lbean.setN_pag(n_pag);
        lbean.setEditorial(editorial);
        lbean.setAutor(abean);

        boolean resp = ldao.insertar(lbean);

        if (resp) {
            msg = "exito al insertar";
        } else {
            msg = "error al insertar";
        }

        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/linsertar.jsp");
        rd.forward(request, response);
    }

    //consultarAll
    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<LibrosBean> lista = ldao.consultarAll();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/lmostrar.jsp");
        rd.forward(request, response);
    }

    //consultarById
    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int ISBN = Integer.parseInt(request.getParameter("ISBN"));

        List<LibrosBean> lista = ldao.consultarById(ISBN);

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/lmodificar.jsp");
        rd.forward(request, response);
    }

    //modificar
    protected void modificar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        int ISBN = Integer.parseInt(request.getParameter("ISBN"));
        String titulo = request.getParameter("titulo");
        Date f_publicacion = formato.parse(request.getParameter("f_publicacion"));
        int n_tomo = Integer.parseInt(request.getParameter("n_tomo"));
        int n_edicion = Integer.parseInt(request.getParameter("n_edicion"));
        String genero = request.getParameter("genero");
        int n_pag = Integer.parseInt(request.getParameter("n_pag"));
        String editorial = request.getParameter("editorial");
        int autor = Integer.parseInt(request.getParameter("autor"));

        LibrosBean lbean = new LibrosBean(ISBN);
        AutoresBean abean = new AutoresBean(autor);
        lbean.setTitulo(titulo);
        lbean.setF_publicacion(f_publicacion);
        lbean.setN_tomo(n_tomo);
        lbean.setN_edicion(n_edicion);
        lbean.setGenero(genero);
        lbean.setN_pag(n_pag);
        lbean.setEditorial(editorial);
        lbean.setAutor(abean);

        boolean resp = ldao.modificar(lbean);
        List<LibrosBean> lista = ldao.consultarAll();
        if (resp) {
            msg = "exito al modificar";
        } else {
            msg = "error al modificar";
        }

        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/lmostrar.jsp");
        rd.forward(request, response);
    }

    //eliminar
    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int ISBN = Integer.parseInt(request.getParameter("ISBN"));

        boolean resp = ldao.eliminar(ISBN);
        List<LibrosBean> lista = ldao.consultarAll();
        if (resp) {
            msg = "exito al eliminar";
        } else {
            msg = "error al eliminar";
        }

        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/lmostrar.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(LibrosServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(LibrosServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
