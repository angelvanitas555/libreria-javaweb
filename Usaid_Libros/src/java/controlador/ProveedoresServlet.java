package controlador;

import dao.Conexion;
import dao.ProveedoresDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.ProveedoresBean;

public class ProveedoresServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "modificar":
                modificar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
        }
    }

    //glovales
    Conexion conn = new Conexion();
    ProveedoresDao pdao = new ProveedoresDao(conn);
    String msg = "";
    RequestDispatcher rd;

    //insertar
    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nombre = request.getParameter("nombre");
        String tipo = request.getParameter("tipo");
        String rubro = request.getParameter("rubro");
        String responsable = request.getParameter("responsable");
        int telefono = Integer.parseInt(request.getParameter("telefono"));

        ProveedoresBean pbean = new ProveedoresBean(0);
        pbean.setNombre(nombre);
        pbean.setTipo(tipo);
        pbean.setRubro(rubro);
        pbean.setResponsable(responsable);
        pbean.setTelefono(telefono);

        boolean resp = pdao.insertar(pbean);

        if (resp) {
            msg = "exito al insertar";
        } else {
            msg = "error al insertar";
        }

        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/pinsertar.jsp");
        rd.forward(request, response);
    }

    //consultarAll
    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<ProveedoresBean> lista = pdao.consultarAll();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/pmostrar.jsp");
        rd.forward(request, response);
    }

    //consultarById
    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        List<ProveedoresBean> lista = pdao.consultarById(id);

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/pmodificar.jsp");
        rd.forward(request, response);
    }

    //modificar
    protected void modificar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String nombre = request.getParameter("nombre");
        String tipo = request.getParameter("tipo");
        String rubro = request.getParameter("rubro");
        String responsable = request.getParameter("responsable");
        int telefono = Integer.parseInt(request.getParameter("telefono"));

        ProveedoresBean pbean = new ProveedoresBean(id);
        pbean.setNombre(nombre);
        pbean.setTipo(tipo);
        pbean.setRubro(rubro);
        pbean.setResponsable(responsable);
        pbean.setTelefono(telefono);
        String  msg1="";
        boolean resp = pdao.modificar(pbean);
        List<ProveedoresBean> lista = pdao.consultarAll();
        if (resp) {
            msg = "exito al modificar";
        } else {
           msg1 = "error al modificar";
        }

        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        request.setAttribute("msg1", msg1);
        rd = request.getRequestDispatcher("/pmostrar.jsp");
        rd.forward(request, response);
    }

    //eliminar
    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        
        boolean resp = pdao.eliminar(id);
        List<ProveedoresBean> lista = pdao.consultarAll();
        if (resp) {
            msg = "exito al eliminar";
        } else {
            msg = "error al eliminar";
        }

        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/pmostrar.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
