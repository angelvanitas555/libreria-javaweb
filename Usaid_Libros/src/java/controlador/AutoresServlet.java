package controlador;

import dao.AutoresDao;
import dao.Conexion;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.AutoresBean;
import modelo.ProveedoresBean;


public class AutoresServlet extends HttpServlet {

    
protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "modificar":
                modificar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
        }
    }

    //glovales
    Conexion conn = new Conexion();
    AutoresDao adao = new AutoresDao(conn);
    String msg = "";
    RequestDispatcher rd;
    SimpleDateFormat formato = new SimpleDateFormat("yy/MM/dd");
    //insertar
    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {

        String nombre = request.getParameter("nombre");
        String nacionalidad = request.getParameter("nacionalidad");
        Date f_nac = formato.parse(request.getParameter("f_nac"));

        AutoresBean abean = new AutoresBean(0);
        abean.setNombre(nombre);
        abean.setNacionalidad(nacionalidad);
        abean.setF_nac(f_nac);

        boolean resp = adao.insertar(abean);

        if (resp) {
            msg = "exito al insertar";
        } else {
            msg = "error al insertar";
        }

        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/auinsertar.jsp");
        rd.forward(request, response);
    }

    //consultarAll
    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<AutoresBean> lista = adao.consultarAll();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/aumostrar.jsp");
        rd.forward(request, response);
    }

    //consultarById
    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        List<AutoresBean> lista = adao.consultarById(id);

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/aumodificar.jsp");
        rd.forward(request, response);
    }

    //modificar
    protected void modificar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        int id = Integer.parseInt(request.getParameter("id"));
        String nombre = request.getParameter("nombre");
        String nacionalidad = request.getParameter("nacionalidad");
        Date f_nac = formato.parse(request.getParameter("f_nac"));

        AutoresBean abean = new AutoresBean(id);
        abean.setNombre(nombre);
        abean.setNacionalidad(nacionalidad);
        abean.setF_nac(f_nac);

        boolean resp = adao.modificar(abean);
        List<AutoresBean> lista = adao.consultarAll();
        if (resp) {
            msg = "exito al modificar";
        } else {
            msg = "error al modificar";
        }

        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/aumostrar.jsp");
        rd.forward(request, response);
    }

    //eliminar
    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        
        boolean resp = adao.eliminar(id);
        List<AutoresBean> lista = adao.consultarAll();
        if (resp) {
            msg = "exito al eliminar";
        } else {
            msg = "error al eliminar";
        }

        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/aumostrar.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    try {
        processRequest(request, response);
    } catch (ParseException ex) {
        Logger.getLogger(AutoresServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    try {
        processRequest(request, response);
    } catch (ParseException ex) {
        Logger.getLogger(AutoresServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
}
