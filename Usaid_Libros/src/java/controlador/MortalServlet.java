package controlador;

import dao.Conexion;
import dao.Loggin_admin;
import dao.UsuariosDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.UsuariosBean;

public class MortalServlet extends HttpServlet {

    String jsp,msg;
    RequestDispatcher rd;
    Conexion conn;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            
            case "insertar_mortal":
                insertar_mortal(request, response);
                break;

        }
    }

    protected void insertar_mortal(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String usuario = request.getParameter("usuario");
        String pass = request.getParameter("pass");
        boolean tipo = Boolean.parseBoolean(request.getParameter("tipo"));
        UsuariosDao udao = new UsuariosDao(conn);
        UsuariosBean ubean = new UsuariosBean(0);
        ubean.setUsuario(usuario);
        ubean.setPass(pass);
        ubean.setTipo(tipo);
        boolean res = udao.insertar(ubean);
        jsp = "mortal.jsp";
        if (res) {
            msg = "Exito al insertar usuario mortal";
        } else {
            msg = "ERROR al insertar usuario mortal";
        }

        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);

    }
  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
}
