package controlador;

import dao.AlumnosDao;
import dao.Conexion;
import dao.LibrosDao;
import dao.ProveedoresDao;
import dao.RegistrosDao;
import dao.StockDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.AlumnosBean;
import modelo.LibrosBean;
import modelo.ProveedoresBean;
import modelo.RegistrosBean;
import modelo.StockBean;


public class RegistrosServlet extends HttpServlet {

   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "modificar":
                modificar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
                case "consultarIn":
                consultarIn(request, response);
                break;
        }
    }

    //glovales
    Conexion conn = new Conexion();
    RegistrosDao rdao = new RegistrosDao(conn);
    String msg = "";
    RequestDispatcher rd;
    
    //consultarIn
    protected void consultarIn(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AlumnosDao aldao=new AlumnosDao(conn);
        List<AlumnosBean> lista1 = aldao.consultarAll();
        
        LibrosDao ldao=new LibrosDao(conn);
        List<LibrosBean> lista2 = ldao.consultarAll();
        
        StockDao sdao=new StockDao(conn);
        List<StockBean> lista3 = sdao.consultarAll();
        
        ProveedoresDao pdao=new ProveedoresDao(conn);
        List<ProveedoresBean> lista4 = pdao.consultarAll();
        
        request.setAttribute("lista1", lista1);
        request.setAttribute("lista2", lista2);
        request.setAttribute("lista3", lista3);
        request.setAttribute("lista4", lista4);
        rd = request.getRequestDispatcher("/rinsertar.jsp");
        rd.forward(request, response);
    }

    //insertar
    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int prestamo = Integer.parseInt(request.getParameter("prestamo"));
        int alumno = Integer.parseInt(request.getParameter("alumno"));
        int ISBN = Integer.parseInt(request.getParameter("ISBN"));
        int stock = Integer.parseInt(request.getParameter("stock"));
        int proveedor = Integer.parseInt(request.getParameter("proveedor"));

        RegistrosBean rbean = new RegistrosBean(0);
        AlumnosBean abean = new AlumnosBean(alumno);
        LibrosBean lbean = new LibrosBean(ISBN);
        StockBean sbean = new StockBean(stock);
        ProveedoresBean pbean = new ProveedoresBean(proveedor);
        rbean.setPrestamo(prestamo);
        rbean.setAlumno(abean);
        rbean.setISBN(lbean);
        rbean.setStock(sbean);
        rbean.setProveedor(pbean);
        
        boolean resp = rdao.insertar(rbean);

        if (resp) {
            msg = "exito al insertar";
        } else {
            msg = "error al insertar";
        }

        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/rinsertar.jsp");
        rd.forward(request, response);
    }

    //consultarAll
    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<RegistrosBean> lista = rdao.consultarAll();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/rmostrar.jsp");
        rd.forward(request, response);
    }

    //consultarById
    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        List<RegistrosBean> lista = rdao.consultarById(id);

        AlumnosDao aldao=new AlumnosDao(conn);
        List<AlumnosBean> lista1 = aldao.consultarAll();
        
        LibrosDao ldao=new LibrosDao(conn);
        List<LibrosBean> lista2 = ldao.consultarAll();
        
        StockDao sdao=new StockDao(conn);
        List<StockBean> lista3 = sdao.consultarAll();
        
        ProveedoresDao pdao=new ProveedoresDao(conn);
        List<ProveedoresBean> lista4 = pdao.consultarAll();
        
        request.setAttribute("lista1", lista1);
        request.setAttribute("lista2", lista2);
        request.setAttribute("lista3", lista3);
        request.setAttribute("lista4", lista4);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/rmodificar.jsp");
        rd.forward(request, response);
    }

    //modificar
    protected void modificar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        int prestamo = Integer.parseInt(request.getParameter("prestamo"));
        int alumno = Integer.parseInt(request.getParameter("alumno"));
        int ISBN = Integer.parseInt(request.getParameter("ISBN"));
        int stock = Integer.parseInt(request.getParameter("stock"));
        int proveedor = Integer.parseInt(request.getParameter("proveedor"));

        RegistrosBean rbean = new RegistrosBean(id);
        AlumnosBean abean = new AlumnosBean(alumno);
        LibrosBean lbean = new LibrosBean(ISBN);
        StockBean sbean = new StockBean(stock);
        ProveedoresBean pbean = new ProveedoresBean(proveedor);
        rbean.setPrestamo(prestamo);
        rbean.setAlumno(abean);
        rbean.setISBN(lbean);
        rbean.setStock(sbean);
        rbean.setProveedor(pbean);

        boolean resp = rdao.modificar(rbean);
        List<RegistrosBean> lista = rdao.consultarAll();
        if (resp) {
            msg = "exito al modificar";
        } else {
            msg = "error al modificar";
        }

        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/rmostrar.jsp");
        rd.forward(request, response);
    }

    //eliminar
    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        
        boolean resp = rdao.eliminar(id);
        List<RegistrosBean> lista = rdao.consultarAll();
        if (resp) {
            msg = "exito al eliminar";
        } else {
            msg = "error al eliminar";
        }

        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/rmostrar.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
