package controlador;

import dao.AdministradoresDao;
import dao.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.AdministradoresBean;
import modelo.UsuariosBean;

public class AdministradoresServlet extends HttpServlet {

    Conexion conn = new Conexion();
    AdministradoresDao adao = new AdministradoresDao(conn);
    String msg, jsp;
    RequestDispatcher rd;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "modificar":
                modificar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "consultarIn":
                consultarIn(request, response);
                break;
        }

    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        int telefono = Integer.parseInt(request.getParameter("telefono"));
        String direccion = request.getParameter("direccion");
        String correo = request.getParameter("correo");
        int cod_usuario = Integer.parseInt(request.getParameter("cod_usuario"));
        String pass_admin = request.getParameter("pass_admin");

        AdministradoresBean abean = new AdministradoresBean(0);
        UsuariosBean ubean = new UsuariosBean(0);
        
        abean.setNombre(nombre);
        abean.setApellido(apellido);
        abean.setTelefono(telefono);
        abean.setDireccion(direccion);
        abean.setCorreo(correo);
        ubean.setId(cod_usuario);
        abean.setCod_usuario(ubean);
        abean.setPass_admin(pass_admin);

        jsp = "registro_administrador.jsp";
        boolean res = adao.insertar(abean);
        if (res) {
            msg = "Exito al ingresar Administrador";
        } else {
            msg = "ERROR al ingresar Administrador";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);
    }

    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        jsp = "admin_table.jsp";
        List<AdministradoresBean> lista = adao.consultarAll();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);

    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        jsp = "admin_table.jsp";
        List<AdministradoresBean> lista = adao.consultarAll();
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);

    }

    protected void modificar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        int telefono = Integer.parseInt(request.getParameter("telefono"));
        String direccion = request.getParameter("direccion");
        String correo = request.getParameter("correo");
        int cod_usuario = Integer.parseInt(request.getParameter("cod_usuario"));
        String pass_admin = request.getParameter("pass_admin");

        AdministradoresBean abean = new AdministradoresBean(id);
        UsuariosBean ubean = new UsuariosBean(cod_usuario);
        abean.setNombre(nombre);
        abean.setApellido(apellido);
        abean.setTelefono(telefono);
        abean.setDireccion(direccion);
        abean.setCorreo(correo);
        abean.setCod_usuario(ubean);
        abean.setPass_admin(pass_admin);

        jsp = "admin_table.jsp";
        boolean res = adao.insertar(abean);
        if (res) {
            msg = "Exito al ingresar Administrador";
        } else {
            msg = "ERROR al ingresar Administrador";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);

    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        boolean res = adao.eliminar(id);
        List<AdministradoresBean> lista = adao.consultarAll();
        jsp = "admin_table.jsp";
        if (res) {
            msg = "Exito al Eliminar Administrador";
        } else {
            msg = "ERROR al Eliminar Administrador";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);
    }

    protected void consultarIn(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        jsp = "registro_administrador.jsp";
        List<UsuariosBean> lista = adao.consultarAlluser();
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
