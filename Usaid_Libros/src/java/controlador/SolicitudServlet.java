package controlador;

import dao.Conexion;
import dao.SolicitudDao;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.AlumnosBean;
import modelo.LibrosBean;
import modelo.SolicitudBean;

public class SolicitudServlet extends HttpServlet {

    Conexion conn = new Conexion();
    SolicitudDao sdao = new SolicitudDao(conn);
    SimpleDateFormat formato = new SimpleDateFormat("yy/MM/dd");
    RequestDispatcher rd;
    String msg, jsp;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "modificar":
                modificar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {

        int id_alumno = Integer.parseInt(request.getParameter("id_alumno"));
        int id_libro = Integer.parseInt(request.getParameter("id_libro"));
        Date fecha_solicitud = formato.parse(request.getParameter("fecha_solicitud"));
        AlumnosBean abean = new AlumnosBean(0);
        LibrosBean lbean = new LibrosBean(0);
        SolicitudBean sbean = new SolicitudBean(0);
        abean.setId(id_alumno);
        sbean.setId_alumno(abean);
        lbean.setISBN(id_libro);
        sbean.setId_libro(lbean);
        sbean.setF_h_solicitud(fecha_solicitud);

        boolean res = sdao.insertar(sbean);
        jsp = "isoli.jsp";
        if (res) {
            msg = "Exito en insertar registro";
        } else {
            msg = "Error en insertar registro";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);
    }

    protected void modificar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        int id = Integer.parseInt(request.getParameter("id"));
        int id_alumno = Integer.parseInt(request.getParameter("id_alumno"));
        int id_libro = Integer.parseInt(request.getParameter("id_libro"));
        Date fecha_solicitud = formato.parse(request.getParameter("fecha_solicitud"));
        AlumnosBean abean = new AlumnosBean(0);
        LibrosBean lbean = new LibrosBean(0);
        SolicitudBean sbean = new SolicitudBean(0);
        abean.setId(id_alumno);
        sbean.setId_alumno(abean);
        lbean.setISBN(id_libro);
        sbean.setId_libro(lbean);
        sbean.setF_h_solicitud(fecha_solicitud);

        boolean res = sdao.modificar(sbean);
        jsp = "msoli.jsp";
        if (res) {
            msg = "Exito en modificar registro";
        } else {
            msg = "Errir en modificar registro";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);

    }

    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        jsp = "msoli.jsp";
        List<SolicitudBean> lista = sdao.consultarAll();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);
    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        jsp = "fmodisoli.jsp";
        List<SolicitudBean> lista = sdao.consultarById(id);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);

    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int id = Integer.parseInt(request.getParameter("id"));
        boolean res = sdao.eliminarById(id);
        List<SolicitudBean> lista = sdao.consultarAll();
        jsp="msoli.jsp";
        if (res) {
            msg = "Exito en eliminar registro";
        } else {
            msg = "ERROR en eliminar registro";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(SolicitudServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(SolicitudServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
