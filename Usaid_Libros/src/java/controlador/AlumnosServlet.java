package controlador;

import dao.AlumnosDao;
import dao.Conexion;
import dao.UsuariosDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.AlumnosBean;
import modelo.ProveedoresBean;
import modelo.UsuariosBean;


public class AlumnosServlet extends HttpServlet {

 protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "modificar":
                modificar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
            case "consultarIn":
                consultarIn(request, response);
                break;
        }
    }

    //glovales
    Conexion conn = new Conexion();
    AlumnosDao adao = new AlumnosDao(conn);
    String msg = "";
    RequestDispatcher rd;

    //insertar
    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nombre = request.getParameter("nombre");
        String carrera = request.getParameter("carrera");
        int cod_usuario = Integer.parseInt(request.getParameter("cod_usuario"));

        UsuariosBean ubean = new UsuariosBean(0);
        AlumnosBean abean = new AlumnosBean(0);
        abean.setNombre(nombre);
        abean.setCarrera(carrera);
        ubean.setId(cod_usuario);
        abean.setCod_usuario(ubean);

        boolean resp = adao.insertar(abean);

        if (resp) {
            msg = "exito al insertar";
        } else {
            msg = "error al insertar";
        }

        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/alinsertar.jsp");
        rd.forward(request, response);
    }

    //consultarAll
    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<AlumnosBean> lista = adao.consultarAll();

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/almostrar.jsp");
        rd.forward(request, response);
    }

    //consultarById
    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        List<AlumnosBean> lista = adao.consultarById(id);

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/almodificar.jsp");
        rd.forward(request, response);
    }

    //modificar
    protected void modificar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String nombre = request.getParameter("nombre");
        String carrera = request.getParameter("carrera");
        int con_usuario = Integer.parseInt(request.getParameter("cod_usuario"));

        AlumnosBean abean = new AlumnosBean(id);
        UsuariosBean ubean = new UsuariosBean(0);
        abean.setNombre(nombre);
        abean.setCarrera(carrera);
        ubean.setId(con_usuario);
        abean.setCod_usuario(ubean);

        boolean resp = adao.modificar(abean);
        List<AlumnosBean> lista = adao.consultarAll();
        if (resp) {
            msg = "exito al modificar";
        } else {
            msg = "error al modificar";
        }

        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/almostrar.jsp");
        rd.forward(request, response);
    }

    //eliminar
    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        
        boolean resp = adao.eliminar(id);
        List<AlumnosBean> lista = adao.consultarAll();
        if (resp) {
            msg = "exito al eliminar";
        } else {
            msg = "error al eliminar";
        }

        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/almostrar.jsp");
        rd.forward(request, response);
    }
    
    protected void consultarIn(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        UsuariosDao udao=new UsuariosDao(conn);
        List<UsuariosBean> lista1 = udao.consultarAll();
        
        request.setAttribute("lista1", lista1);
        rd = request.getRequestDispatcher("/alinsertar.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    
}
