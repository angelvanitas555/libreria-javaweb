package controlador;

import dao.Conexion;
import dao.UsuariosDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.UsuariosBean;

public class UsuariosServlet extends HttpServlet {

    Conexion conn = new Conexion();
    UsuariosDao udao = new UsuariosDao(conn);
    String msg = "", jsp = "";
    RequestDispatcher rd;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "modificar":
                modificar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
        }
    }

    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String usuario = request.getParameter("usuario");
        String pass = request.getParameter("pass");
        boolean tipo = Boolean.parseBoolean(request.getParameter("tipo"));

        UsuariosBean ubean = new UsuariosBean(0);
        ubean.setUsuario(usuario);
        ubean.setPass(pass);
        ubean.setTipo(tipo);
        boolean res = udao.insertar(ubean);
        jsp = "uinsertar.jsp";
        if (res) {
            msg = "Exito al insertar registro";
        } else {
            msg = "ERROR al insertar registro";
        }

        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);

    }

    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         jsp = "umostrar.jsp";
        
        List<UsuariosBean> lista = udao.consultarAll();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);

    }

    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        List<UsuariosBean> lista = udao.consultarById(id);
        jsp = "umodificar.jsp";
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);

    }

    protected void modificar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int id = Integer.parseInt(request.getParameter("id"));
        String usuario = request.getParameter("usuario");
        String pass = request.getParameter("pass");
        boolean tipo = Boolean.parseBoolean(request.getParameter("tipo"));

        UsuariosBean ubean = new UsuariosBean(id);
        ubean.setUsuario(usuario);
        ubean.setPass(pass);
        ubean.setTipo(tipo);

        boolean res = udao.modificar(ubean);
        List<UsuariosBean> lista = udao.consultarAll();
        jsp = "umostrar.jsp";
        if (res) {
            msg = "Exito al modificar registro";
        } else {
            msg = "ERROR al modificar registro";
        }
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);
    }

    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int id = Integer.parseInt(request.getParameter("id"));
        boolean res = udao.eliminar(id);
        List<UsuariosBean> lista = udao.consultarAll();
        jsp = "umostrar.jsp";
        if (res) {
            msg = "Exito al eliminar registro";
        } else {
            msg = "ERROR al eliminar registro";
        }
        
        request.setAttribute("msg", msg);
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher(jsp);
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
