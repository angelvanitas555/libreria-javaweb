package controlador;

import dao.Conexion;
import dao.LibrosDao;
import dao.StockDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.LibrosBean;
import modelo.StockBean;


public class StockServlet extends HttpServlet {
protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insertar":
                insertar(request, response);
                break;
            case "consultarAll":
                consultarAll(request, response);
                break;
            case "consultarById":
                consultarById(request, response);
                break;
            case "modificar":
                modificar(request, response);
                break;
            case "eliminar":
                eliminar(request, response);
                break;
                case "consultarIn":
                consultarIn(request, response);
                break;
        }
    }

    //glovales
    Conexion conn = new Conexion();
    StockDao sdao = new StockDao(conn);
    String msg = "";
    RequestDispatcher rd;

    //consultarIn
    protected void consultarIn(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LibrosDao ldao=new LibrosDao(conn);
        List<LibrosBean> lista = ldao.consultarAll();
        
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/sinsertar.jsp");
        rd.forward(request, response);
    }
    
    //insertar
    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int ISBN = Integer.parseInt(request.getParameter("ISBN"));
        int disponibles = Integer.parseInt(request.getParameter("disponibles"));
        int stock = Integer.parseInt(request.getParameter("stock"));
        boolean existencia = Boolean.parseBoolean(request.getParameter("existencia"));
        int dias_limite = Integer.parseInt(request.getParameter("dias_limite"));

        StockBean sbean = new StockBean(0);
        LibrosBean lbean = new LibrosBean(ISBN);
        sbean.setISBN(lbean);
        sbean.setDisponibles(disponibles);
        sbean.setStock(stock);
        sbean.setExistencia(existencia);
        sbean.setDias_limite(dias_limite);

        boolean resp = sdao.insertar(sbean);

        if (resp) {
            msg = "exito al insertar";
        } else {
            msg = "error al insertar";
        }

        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/sinsertar.jsp");
        rd.forward(request, response);
    }

    //consultarAll
    protected void consultarAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<StockBean> lista = sdao.consultarAll();
        
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/smostrar.jsp");
        rd.forward(request, response);
    }

    //consultarById
    protected void consultarById(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        List<StockBean> lista = sdao.consultarById(id);

        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/smodificar.jsp");
        rd.forward(request, response);
    }

    //modificar
    protected void modificar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        int ISBN = Integer.parseInt(request.getParameter("ISBN"));
        int disponibles = Integer.parseInt(request.getParameter("disponibles"));
        int stock = Integer.parseInt(request.getParameter("stock"));
        boolean existencia = Boolean.parseBoolean(request.getParameter("existencia"));
        int dias_limite = Integer.parseInt(request.getParameter("dias_limite"));

        StockBean sbean = new StockBean(id);
        LibrosBean lbean = new LibrosBean(ISBN);
        sbean.setISBN(lbean);
        sbean.setDisponibles(disponibles);
        sbean.setStock(stock);
        sbean.setExistencia(existencia);
        sbean.setDias_limite(dias_limite);

        boolean resp = sdao.modificar(sbean);
        List<StockBean> lista = sdao.consultarAll();
        if (resp) {
            msg = "exito al modificar";
        } else {
            msg = "error al modificar";
        }

        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/smostrar.jsp");
        rd.forward(request, response);
    }

    //eliminar
    protected void eliminar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        
        boolean resp = sdao.eliminar(id);
        List<StockBean> lista = sdao.consultarAll();
        if (resp) {
            msg = "exito al eliminar";
        } else {
            msg = "error al eliminar";
        }

        request.setAttribute("lista", lista);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/smostrar.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
