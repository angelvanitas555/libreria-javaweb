package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.UsuariosBean;

public class UsuariosDao {

    PreparedStatement ps;
    ResultSet rs;
    Conexion conn;

    public UsuariosDao(Conexion conn) {
        this.conn = conn;
    }

    //metodo para insertar datos en la tabla usuarios
    public boolean insertar(UsuariosBean ubean) {
        String sql = "insert into usuarios values(?,?,?,?)";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, ubean.getId());
            ps.setString(2, ubean.getUsuario());
            ps.setString(3, ubean.getPass());
            ps.setBoolean(4, ubean.getTipo());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //metodo para consultar todo
    public List<UsuariosBean> consultarAll() {
        String sql = "select * from usuarios";
        try {
            ps = conn.conectar().prepareStatement(sql);
            rs = ps.executeQuery();
            UsuariosBean ubean;
            List<UsuariosBean> lista = new LinkedList<>();
            while (rs.next()) {
                ubean = new UsuariosBean(rs.getInt("id"));
                ubean.setUsuario(rs.getString("usuario"));
                ubean.setPass(rs.getString("pass"));
                ubean.setTipo(rs.getBoolean("tipo"));
                lista.add(ubean);
            }
            return lista;
        } catch (Exception e) {
            return null;
            
        }
    }

    //metodo para consultar un registro por su id
    public List<UsuariosBean> consultarById(int id) {
        String sql = "select * from usuarios where id=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            List<UsuariosBean> lista = new LinkedList<>();
            UsuariosBean ubean;
            while (rs.next()) {
                ubean = new UsuariosBean(rs.getInt("id"));
                ubean.setUsuario(rs.getString("usuario"));
                ubean.setPass(rs.getString("pass"));
                ubean.setTipo(rs.getBoolean("tipo"));
                lista.add(ubean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    //Metodo para modificar un registro de la base de datos
    public boolean modificar(UsuariosBean ubean) {
        String sql = "update  usuarios set usuario=?,pass=?,tipo=? where id=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, ubean.getUsuario());
            ps.setString(2, ubean.getPass());
            ps.setBoolean(3, ubean.getTipo());
            ps.setInt(4, ubean.getId());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //metodo para eliminar un registro con su id como parámetro
    public boolean eliminar(int id) {
        String sql = "delete from usuarios where id=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
