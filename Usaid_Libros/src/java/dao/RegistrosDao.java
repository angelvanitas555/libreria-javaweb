
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.AlumnosBean;
import modelo.LibrosBean;
import modelo.ProveedoresBean;
import modelo.RegistrosBean;
import modelo.StockBean;

/**
 *
 * @author juan.serranoUSAM
 */
public class RegistrosDao {
     Conexion conn;

    public RegistrosDao(Conexion conn) {
        this.conn = conn;
    }
    
    //metodo insertar
    public boolean insertar(RegistrosBean rbean){
        String sql="insert into registros values (?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, rbean.getId());
            ps.setInt(2, rbean.getPrestamo());
            ps.setInt(3, rbean.getAlumno().getId());
            ps.setInt(4, rbean.getISBN().getISBN());
            ps.setInt(5, rbean.getStock().getId());
            ps.setInt(6, rbean.getProveedor().getId());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    //metodo consultar todo
    public List<RegistrosBean> consultarAll(){
        String sql="SELECT r.id,r.prestamo,al.nombre as nombreal,l.titulo,s.disponibles,p.nombre \n" +
"FROM registros as r inner join alumnos as al on r.id=al.id inner join libros as l on r.id=l.ISBN \n" +
"inner join stock as s on r.id=s.id inner join proveedores as p on r.id=p.id";
        try {
           PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            RegistrosBean rbean;
            LibrosBean lbean = new LibrosBean(0);
            ProveedoresBean pbean = new ProveedoresBean(0);
            StockBean sbean = new StockBean(0);
            AlumnosBean abean = new AlumnosBean(0);
            List<RegistrosBean> lista = new LinkedList<>();
            while (rs.next()) {                
                rbean = new RegistrosBean(rs.getInt("id"));
                rbean.setPrestamo(rs.getInt("prestamo"));
                abean.setNombre(rs.getString("nombreal"));
                lbean.setTitulo(rs.getString("titulo"));
                sbean.setDisponibles(rs.getInt("disponibles"));
                pbean.setNombre(rs.getString("nombre"));
                rbean.setAlumno(abean);
                rbean.setISBN(lbean);
                rbean.setStock(sbean);
                rbean.setProveedor(pbean);
                lista.add(rbean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
    //metodo consultar por id
    public List<RegistrosBean> consultarById(int id){
        String sql="select*from registros where id=?";
        try {
           PreparedStatement ps = conn.conectar().prepareStatement(sql);
           ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            RegistrosBean rbean;
            LibrosBean lbean = new LibrosBean(0);
            ProveedoresBean pbean = new ProveedoresBean(0);
            StockBean sbean = new StockBean(0);
            AlumnosBean abean = new AlumnosBean(0);
            List<RegistrosBean> lista = new LinkedList<>();
            while (rs.next()) {                
                rbean = new RegistrosBean(rs.getInt("id"));
                rbean.setPrestamo(rs.getInt("prestamo"));
                abean.setId(rs.getInt("alumno"));
                lbean.setISBN(rs.getInt("ISBN"));
                sbean.setId(rs.getInt("stock"));
                pbean.setId(rs.getInt("proveedor"));
                rbean.setAlumno(abean);
                rbean.setISBN(lbean);
                rbean.setStock(sbean);
                rbean.setProveedor(pbean);
                lista.add(rbean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
     //metodo modificar
    public boolean modificar(RegistrosBean rbean){
        String sql="update registros set prestamo=?, alumno=?, ISBN=?, stock=?, proveedor=? where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, rbean.getPrestamo());
            ps.setInt(2, rbean.getAlumno().getId());
            ps.setInt(3, rbean.getISBN().getISBN());
            ps.setInt(4, rbean.getStock().getId());
            ps.setInt(5, rbean.getProveedor().getId());
           ps.setInt(6, rbean.getId());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    //metodo eliminar
    public boolean eliminar(int id){
        String sql="delete from registros where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
