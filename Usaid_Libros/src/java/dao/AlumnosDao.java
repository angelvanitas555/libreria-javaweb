package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.AlumnosBean;
import modelo.UsuariosBean;

/**
 *
 * @author juan.serranoUSAM
 */
public class AlumnosDao {

    Conexion conn;

    public AlumnosDao(Conexion conn) {
        this.conn = conn;
    }

    //metodo insertar
    public boolean insertar(AlumnosBean abean) {
        String sql = "insert into alumnos values (?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, abean.getId());
            ps.setString(2, abean.getNombre());
            ps.setString(3, abean.getCarrera());
            ps.setInt(4, abean.getCod_usuario().getId());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //metodo consultar todo
    public List<AlumnosBean> consultarAll() {
        String sql = "select*from alumnos";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            AlumnosBean abean;

            List<AlumnosBean> lista = new LinkedList<>();
            while (rs.next()) {
                UsuariosBean ubean = new UsuariosBean(0);
                abean = new AlumnosBean(rs.getInt("id"));
                abean.setNombre(rs.getString("nombre"));
                abean.setCarrera(rs.getString("carrera"));
                ubean.setId(rs.getInt("cod_usuario"));
                abean.setCod_usuario(ubean);
                lista.add(abean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    //metodo consultar por id
    public List<AlumnosBean> consultarById(int id) {
        String sql = "select*from alumnos where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            AlumnosBean abean;
            UsuariosBean ubean = new UsuariosBean(0);
            List<AlumnosBean> lista = new LinkedList<>();
            while (rs.next()) {
                abean = new AlumnosBean(rs.getInt("id"));
                abean.setNombre(rs.getString("nombre"));
                abean.setCarrera(rs.getString("carrera"));
                ubean.setId(rs.getInt("cod_usuario"));
                abean.setCod_usuario(ubean);
                lista.add(abean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    //metodo modificar
    public boolean modificar(AlumnosBean abean) {
        String sql = "update alumnos set nombre=?, carrera=?,cod_usuario=? where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, abean.getNombre());
            ps.setString(2, abean.getCarrera());
            ps.setInt(3, abean.getCod_usuario().getId());
            ps.setInt(4, abean.getId());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //metodo eliminar
    public boolean eliminar(int id) {
        String sql = "delete from alumnos where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
