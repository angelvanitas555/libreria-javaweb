package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.AdministradoresBean;
import modelo.UsuariosBean;

public class AdministradoresDao {

    Conexion conn;
    PreparedStatement ps;
    ResultSet rs;

    public AdministradoresDao(Conexion conn) {
        this.conn = conn;
    }

    //metodo para insertar un registro en la tabla
    public boolean insertar(AdministradoresBean abean) {
        String sql = "insert into administradores values (?,?,?,?,?,?,?,?)";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, abean.getId());
            ps.setString(2, abean.getNombre());
            ps.setString(3, abean.getApellido());
            ps.setInt(4, abean.getTelefono());
            ps.setString(5, abean.getDireccion());
            ps.setString(6, abean.getCorreo());
            ps.setInt(7, abean.getCod_usuario().getId());
            ps.setString(8, abean.getPass_admin());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //metodo para consultar todos los registros de la base
    public List<AdministradoresBean> consultarAll() {
        String sql = "select * from administradores";
        try {
            ps = conn.conectar().prepareStatement(sql);
            rs = ps.executeQuery();
            List<AdministradoresBean> lista = new LinkedList<>();
            AdministradoresBean abean;
            UsuariosBean ubean = new UsuariosBean(0);
            while (rs.next()) {
                abean = new AdministradoresBean(rs.getInt("id"));
                abean.setNombre(rs.getString("nombre"));
                abean.setApellido(rs.getString("apellido"));
                abean.setTelefono(rs.getInt("telefono"));
                abean.setDireccion(rs.getString("direccion"));
                abean.setCorreo(rs.getString("correo"));
                ubean.setId(rs.getInt("cod_usuario"));
                abean.setCod_usuario(ubean);
                abean.setPass_admin(rs.getString("pass_admin"));

                lista.add(abean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    //metodo para consultar un registro por su id como parámetro
    public List<AdministradoresBean> consutlarById(int id) {
        String sql = "select * from administradores where id=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            rs = ps.executeQuery();
            List<AdministradoresBean> lista = new LinkedList<>();
            AdministradoresBean abean;
            UsuariosBean ubean = new UsuariosBean(0);
            while (rs.next()) {
                abean = new AdministradoresBean(rs.getInt("id"));
                abean.setNombre(rs.getString("nombre"));
                abean.setApellido(rs.getString("apellido"));
                abean.setTelefono(rs.getInt("telefono"));
                abean.setDireccion(rs.getString("direccion"));
                abean.setCorreo(rs.getString("correo"));
                ubean.setId(rs.getInt("cod_usuario"));
                abean.setCod_usuario(ubean);
                abean.setPass_admin(rs.getString("pass_admin"));

                lista.add(abean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    //metodo para modificar un registro 
    public boolean modificar(AdministradoresBean abean) {
        String sql = "update administradores set nombre=?, apellido=?, telefono=?,"
                + "direccion=?, correo=?, cod_usuario=? ,pass_admin=?  where id=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, abean.getNombre());
            ps.setString(2, abean.getApellido());
            ps.setInt(3, abean.getTelefono());
            ps.setString(4, abean.getDireccion());
            ps.setString(5, abean.getCorreo());
            ps.setInt(6, abean.getCod_usuario().getId());
            ps.setString(7, abean.getPass_admin());
            ps.setInt(8, abean.getId());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //metodo para eliminar un registro con id como parámetro
    public boolean eliminar(int id) {
        String sql = "delete from administradores where id=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    //merer
    public List<UsuariosBean> consultarAlluser() {
        String sql = "select * from usuarios where tipo=1";
        try {
            ps = conn.conectar().prepareStatement(sql);
            rs = ps.executeQuery();
            UsuariosBean ubean;
            List<UsuariosBean> lista = new LinkedList<>();
            while (rs.next()) {
                ubean = new UsuariosBean(rs.getInt("id"));
                ubean.setUsuario(rs.getString("usuario"));
                ubean.setPass(rs.getString("pass"));
                ubean.setTipo(rs.getBoolean("tipo"));
                lista.add(ubean);
            }
            return lista;
        } catch (Exception e) {
            return null;
            
        }
    }

}
