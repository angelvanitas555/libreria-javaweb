package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.AdministradoresBean;
import modelo.UsuariosBean;

public class Loggin_admin {

    Conexion conn;
    String sql = "";
    PreparedStatement ps;
    ResultSet rs;

    public Loggin_admin(Conexion conn) {
        this.conn = conn;
    }
    
    

    //metodo para registrar a un administrador para el sistema
    public boolean Autenticacion(String nombre,String pass_admin) {
        sql = "select * from administradores where nombre=? and pass_admin=?";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, nombre);
            ps.setString(2, pass_admin);
            rs = ps.executeQuery();
            if (rs.absolute(1)) {
                return true;
            }
            System.out.println("Se puedo ingresar REGISTRO CORRECTAMENTE");
        } catch (Exception e) {
            System.err.println("no se pudo seleccionar el usuario y pass" + "\n" + "REVISA EL SQL" + "\n" + e);
            return false;
        } finally {
            try {
                if (conn != null) {
                    conn.desconectar();
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                System.err.println("No se pudo cerrar la conexion" + e);
            }
        }
        return false;
    }

    //metodo para ingresar a administrador
    public boolean registrar_admin(AdministradoresBean abean) {
        String sql = "insert into administradores values(?,?,?,?,?,?,?,?,?)";
        try {
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, abean.getId());
            ps.setString(2, abean.getNombre());
            ps.setString(3, abean.getApellido());
            ps.setInt(4, abean.getTelefono());
            ps.setString(5, abean.getDireccion());
            ps.setString(6, abean.getCorreo());
            ps.setInt(7, abean.getCod_usuario().getId());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    //metodo para consultar registro ESTE ES EL QUE OCUPA EL PROGRAMA
    public UsuariosBean loggin(String nombre,String pass){
    sql = "select * from usuarios where usuario=? && pass=?";
        try {
           
            ps = conn.conectar().prepareStatement(sql);
            ps.setString(1,nombre);
            ps.setString(2, pass);
            rs = ps.executeQuery();
            UsuariosBean ubean = null;
            
            while(rs.next()){
                ubean = new UsuariosBean(rs.getInt("id"));
            ubean.setUsuario(rs.getString("usuario"));
            ubean.setPass(rs.getString("pass"));
            ubean.setTipo(rs.getBoolean("tipo"));
            }return ubean;
        } catch (Exception e) {return null;
        }
    }
}
