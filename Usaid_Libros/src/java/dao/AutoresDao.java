
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import modelo.AutoresBean;

/**
 *
 * @author juan.serranoUSAM
 */
public class AutoresDao {
    Conexion conn;
    SimpleDateFormat formato = new SimpleDateFormat("yy/MM/dd");

    public AutoresDao(Conexion conn) {
        this.conn = conn;
    }
    
    //metodo insertar
    public boolean insertar(AutoresBean abean){
        String sql="insert into autores values (?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, abean.getId());
            ps.setString(2, abean.getNombre());
            ps.setString(3, abean.getNacionalidad());
            ps.setString(4, formato.format(abean.getF_nac()));
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    //metodo consultar todo
    public List<AutoresBean> consultarAll(){
        String sql="select*from autores";
        try {
           PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            AutoresBean abean;
            List<AutoresBean> lista = new LinkedList<>();
            while (rs.next()) {                
                abean = new AutoresBean(rs.getInt("id"));
                abean.setNombre(rs.getString("nombre"));
                abean.setNacionalidad(rs.getString("nacionalidad"));
                abean.setF_nac(rs.getDate("f_nac"));
                lista.add(abean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
    //metodo consultar por id
    public List<AutoresBean> consultarById(int id){
        String sql="select*from autores where id=?";
        try {
           PreparedStatement ps = conn.conectar().prepareStatement(sql);
           ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            AutoresBean abean;
            List<AutoresBean> lista = new LinkedList<>();
            while (rs.next()) {                
                abean = new AutoresBean(rs.getInt("id"));
                abean.setNombre(rs.getString("nombre"));
                abean.setNacionalidad(rs.getString("nacionalidad"));
                abean.setF_nac(rs.getDate("f_nac"));
                lista.add(abean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
     //metodo modificar
    public boolean modificar(AutoresBean abean){
        String sql="update autores set nombre=?, nacionalidad=?, f_nac=? where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, abean.getId());
            ps.setString(2, abean.getNombre());
            ps.setString(3, abean.getNacionalidad());
            ps.setString(4, formato.format(abean.getF_nac()));
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    //metodo eliminar
    public boolean eliminar(int id){
        String sql="delete from autores where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
