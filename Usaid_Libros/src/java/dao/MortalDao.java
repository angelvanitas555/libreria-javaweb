
package dao;

import java.sql.PreparedStatement;
import modelo.UsuariosBean;


public class MortalDao {
    Conexion conn;

    public MortalDao(Conexion conn) {
        this.conn = conn;
    }
    
    //metodo para insertar datos en la tabla usuarios
    public boolean insertar_mortal(UsuariosBean ubean) {
        String sql = "insert into usuarios values(?,?,?,?)";
        try {
            PreparedStatement ps;
            ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, ubean.getId());
            ps.setString(2, ubean.getUsuario());
            ps.setString(3, ubean.getPass());
            ps.setBoolean(4, false);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
