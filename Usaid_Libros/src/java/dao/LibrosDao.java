
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import modelo.AutoresBean;
import modelo.LibrosBean;

/**
 *
 * @author juan.serranoUSAM
 */
public class LibrosDao {
    Conexion conn;
    SimpleDateFormat formato = new SimpleDateFormat("yy/MM/dd");

    public LibrosDao(Conexion conn) {
        this.conn = conn;
    }
    
    //metodo insertar
    public boolean insertar(LibrosBean lbean){
        String sql="insert into libros values (?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, lbean.getISBN());
            ps.setString(2, lbean.getTitulo());
            ps.setString(3, formato.format(lbean.getF_publicacion()));
            ps.setInt(4, lbean.getN_tomo());
            ps.setInt(5, lbean.getN_edicion());
            ps.setString(6, lbean.getGenero());
            ps.setInt(7, lbean.getN_pag());
            ps.setString(8, lbean.getEditorial());
            ps.setInt(9, lbean.getAutor().getId());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    //metodo consultar todo
    public List<LibrosBean> consultarAll(){
        String sql="select l.ISBN,l.titulo,l.f_publicacion,l.n_tomo,l.n_edicion,\n" +
"l.genero,l.n_pag,l.editorial,au.nombre \n" +
"from libros as l inner join autores as au on au.id=l.autor";
        try {
           PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            LibrosBean lbean;
            AutoresBean abean = new AutoresBean(0);
            List<LibrosBean> lista = new LinkedList<>();
            while (rs.next()) {                
                lbean = new LibrosBean(rs.getInt("ISBN"));
                lbean.setTitulo(rs.getString("titulo"));
                lbean.setF_publicacion(rs.getDate("f_publicacion"));
                lbean.setN_tomo(rs.getInt("n_tomo"));
                lbean.setN_edicion(rs.getInt("n_edicion"));
                lbean.setGenero(rs.getString("genero"));
                lbean.setN_pag(rs.getInt("n_pag"));
                lbean.setEditorial(rs.getString("editorial"));
                abean.setNombre(rs.getString("nombre"));
                lbean.setAutor(abean);
                lista.add(lbean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
    //metodo consultar por id
    public List<LibrosBean> consultarById(int id){
        String sql="select*from libros where ISBN=?";
        try {
           PreparedStatement ps = conn.conectar().prepareStatement(sql);
           ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            LibrosBean lbean;
            AutoresBean abean = new AutoresBean(0);
            List<LibrosBean> lista = new LinkedList<>();
            while (rs.next()) {                
                lbean = new LibrosBean(rs.getInt("ISBN"));
                lbean.setTitulo(rs.getString("titulo"));
                lbean.setF_publicacion(rs.getDate("f_publicacion"));
                lbean.setN_tomo(rs.getInt("n_tomo"));
                lbean.setN_edicion(rs.getInt("n_edicion"));
                lbean.setGenero(rs.getString("genero"));
                lbean.setN_pag(rs.getInt("n_pag"));
                lbean.setEditorial(rs.getString("editorial"));
                abean.setId(rs.getInt("autor"));
                lbean.setAutor(abean);
                lista.add(lbean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
     //metodo modificar
    public boolean modificar(LibrosBean lbean){
        String sql="update libros set titulo=?, f_publicacion=?, n_tomo=?, n_edicion=?, genero=?, n_pag=?, editorial=?, autor=? where ISBN=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, lbean.getTitulo());
            ps.setString(2, formato.format(lbean.getF_publicacion()));
            ps.setInt(3, lbean.getN_tomo());
            ps.setInt(4, lbean.getN_edicion());
            ps.setString(5, lbean.getGenero());
            ps.setInt(6, lbean.getN_pag());
            ps.setString(7, lbean.getEditorial());
            ps.setInt(8, lbean.getAutor().getId());
            ps.setInt(9, lbean.getISBN());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    //metodo eliminar
    public boolean eliminar(int id){
        String sql="delete from libros where ISBN=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
