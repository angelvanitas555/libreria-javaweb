
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.LibrosBean;
import modelo.StockBean;

/**
 *
 * @author juan.serranoUSAM
 */
public class StockDao {
        Conexion conn;

    public StockDao(Conexion conn) {
        this.conn = conn;
    }
    
    //metodo insertar
    public boolean insertar(StockBean sbean){
        String sql="insert into stock values (?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, sbean.getId());
            ps.setInt(2, sbean.getISBN().getISBN());
            ps.setInt(3, sbean.getDisponibles());
            ps.setInt(4, sbean.getStock());
            ps.setBoolean(5, sbean.getExistencia());
            ps.setInt(6, sbean.getDias_limite());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    //metodo consultar todo
    public List<StockBean> consultarAll(){
        String sql="SELECT s.id, l.titulo,"
                + " s.disponibles, s.stock, s.existencia, s.dias_limite "
                + "FROM stock as s inner join libros as l on s.ISBN=l.ISBN;";
        try {
           PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            StockBean sbean;
            LibrosBean lbean = new LibrosBean(0);
            List<StockBean> lista = new LinkedList<>();
            while (rs.next()) {                
                sbean = new StockBean(rs.getInt("id"));
                lbean.setTitulo(rs.getString("titulo"));
                sbean.setISBN(lbean);
                sbean.setDisponibles(rs.getInt("disponibles"));
                sbean.setStock(rs.getInt("stock"));
                sbean.setExistencia(rs.getBoolean("existencia"));
                sbean.setDias_limite(rs.getInt("dias_limite"));
                lista.add(sbean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
    //metodo consultar por id
    public List<StockBean> consultarById(int id){
        String sql="select*from stock where id=?";
        try {
           PreparedStatement ps = conn.conectar().prepareStatement(sql);
           ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            StockBean sbean;
            LibrosBean lbean = new LibrosBean(0);
            List<StockBean> lista = new LinkedList<>();
            while (rs.next()) {                
                sbean = new StockBean(rs.getInt("id"));
                lbean.setISBN(rs.getInt("ISBN"));
                sbean.setISBN(lbean);
                sbean.setDisponibles(rs.getInt("disponibles"));
                sbean.setStock(rs.getInt("stock"));
                sbean.setExistencia(rs.getBoolean("existencia"));
                sbean.setDias_limite(rs.getInt("dias_limite"));
                lista.add(sbean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
     //metodo modificar
    public boolean modificar(StockBean sbean){
        String sql="update stock set ISBN=?, disponibles=?, stock=?, existencia=?, dias_limite=? where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, sbean.getISBN().getISBN());
            ps.setInt(2, sbean.getDisponibles());
            ps.setInt(3, sbean.getStock());
            ps.setBoolean(4, sbean.getExistencia());
            ps.setInt(5, sbean.getDias_limite());
            ps.setInt(6, sbean.getId());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    //metodo eliminar
    public boolean eliminar(int id){
        String sql="delete from stock where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
