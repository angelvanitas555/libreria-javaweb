package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import modelo.EntregaBean;

public class EntregaDao {
    PreparedStatement ps;
    ResultSet rs;
    Conexion conn;

    public EntregaDao(Conexion conn) {
        this.conn = conn;
    }

    //metodo para insertar datos a la tabla entrega
    public List<EntregaBean> consultarAll() {
        String sql = "select id_entrega, nombre,titulo,f_prestamo ,f_limite,fecha_entrega,mora,dias_mora\n"
                + "from entrega as ent inner join alumnos as al on ent.id_alumno=al.id\n"
                + "inner join libros as lib on ent.isbn_libro=lib.ISBN\n"
                + "inner join prestamo as p_uno on ent.fecha_prestamo=p_uno.id";
        try {
            ps = conn.conectar().prepareStatement(sql);
            
        } catch (Exception e) {
        }

    }

}
