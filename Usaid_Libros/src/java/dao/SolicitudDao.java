package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import modelo.AlumnosBean;
import modelo.LibrosBean;
import modelo.SolicitudBean;

public class SolicitudDao {

    Conexion conn;
    SimpleDateFormat formato = new SimpleDateFormat("yy/MM/dd");

    public SolicitudDao(Conexion conn) {
        this.conn = conn;
    }

    //metodo para insertar datos a la db
    public boolean insertar(SolicitudBean sbean) {
        String sql = "insert into solicitud values(?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, sbean.getId());
            ps.setInt(2, sbean.getId_alumno().getId());
            ps.setInt(3, sbean.getId_libro().getISBN());
            ps.setString(4, formato.format(sbean.getF_h_solicitud()));
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //metodo para consultar toda la tabla de la base de datos
    public boolean modificar(SolicitudBean sbean) {
        String sql = "update solicitud set id_alumno=?, id_libro=?, f_h_solicitud=? where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, sbean.getId_alumno().getId());
            ps.setInt(2, sbean.getId_libro().getISBN());
            ps.setString(3, formato.format(sbean.getF_h_solicitud()));
            ps.setInt(4, sbean.getId());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //metodo para consultar todo de la data base
    public List<SolicitudBean> consultarAll() {
        String sql = "select * from solicitud";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            List<SolicitudBean> lista = new LinkedList<>();
            while (rs.next()) {
                SolicitudBean sbean;
                LibrosBean lbean = new LibrosBean(0);
                AlumnosBean abean = new AlumnosBean(0);
                sbean = new SolicitudBean(rs.getInt("id"));
                abean.setId(rs.getInt("id"));
                sbean.setId_alumno(abean);
                lbean.setISBN(rs.getInt("id_libro"));
                sbean.setId_libro(lbean);
                sbean.setF_h_solicitud(rs.getDate("f_h_solicitud"));
                lista.add(sbean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    //metodo para consultar un registro por su id como parámetro
    public List<SolicitudBean> consultarById(int id) {
        String sql = "select * from solicitud where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            SolicitudBean sbean;

            List<SolicitudBean> lista = new LinkedList<>();
            while (rs.next()) {
                AlumnosBean abean = new AlumnosBean(0);
                LibrosBean lbean = new LibrosBean(0);
                sbean = new SolicitudBean(rs.getInt("id"));
                abean.setId(rs.getInt("id"));
                sbean.setId_alumno(abean);
                lbean.setISBN(rs.getInt("id_libro"));
                sbean.setF_h_solicitud(rs.getDate("f_h_solicitud"));
                lista.add(sbean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    //metodo para eliinar un registro de la base de datos
    public boolean eliminarById(int id) {
        String sql = "delete from solicitud where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
