
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.ProveedoresBean;

/**
 *
 * @author juan.serranoUSAM
 */
public class ProveedoresDao {
    
    Conexion conn;

    public ProveedoresDao(Conexion conn) {
        this.conn = conn;
    }
    
    //metodo insertar
    public boolean insertar(ProveedoresBean pbean){
        String sql="insert into proveedores value (?,?,?,?,?,?)";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, pbean.getId());
            ps.setString(2, pbean.getNombre());
            ps.setString(3, pbean.getTipo());
            ps.setString(4, pbean.getRubro());
            ps.setString(5, pbean.getResponsable());
            ps.setInt(6, pbean.getTelefono());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    //metodo consultar todo
    public List<ProveedoresBean> consultarAll(){
        String sql="select*from proveedores";
        try {
           PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ProveedoresBean pbean;
            List<ProveedoresBean> lista = new LinkedList<>();
            while (rs.next()) {                
                pbean = new ProveedoresBean(rs.getInt("id"));
                pbean.setNombre(rs.getString("nombre"));
                pbean.setTipo(rs.getString("tipo"));
                pbean.setRubro(rs.getString("rubro"));
                pbean.setResponsable(rs.getString("responsable"));
                pbean.setTelefono(rs.getInt("telefono"));
                lista.add(pbean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
    //metodo consultar por id
    public List<ProveedoresBean> consultarById(int id){
        String sql="select*from proveedores where id=?";
        try {
           PreparedStatement ps = conn.conectar().prepareStatement(sql);
           ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            ProveedoresBean pbean;
            List<ProveedoresBean> lista = new LinkedList<>();
            while (rs.next()) {                
                pbean = new ProveedoresBean(rs.getInt("id"));
                pbean.setNombre(rs.getString("nombre"));
                pbean.setTipo(rs.getString("tipo"));
                pbean.setRubro(rs.getString("rubro"));
                pbean.setResponsable(rs.getString("responsable"));
                pbean.setTelefono(rs.getInt("telefono"));
                lista.add(pbean);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
     //metodo modificar
    public boolean modificar(ProveedoresBean pbean){
        String sql="update proveedores set nombre=?, tipo=?, rubro=?, responsable=?, telefono=? where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setString(1, pbean.getNombre());
            ps.setString(2, pbean.getTipo());
            ps.setString(3, pbean.getRubro());
            ps.setString(4, pbean.getResponsable());
            ps.setInt(5, pbean.getTelefono());
            ps.setInt(6, pbean.getId());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    //metodo eliminar
    public boolean eliminar(int id){
        String sql="delete from proveedores where id=?";
        try {
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
