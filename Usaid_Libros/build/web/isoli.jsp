<%-- 
    Document   : isoli
    Created on : 02-07-2019, 09:02:11 AM
    Author     : jose.azucenaUSAM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="materialize/css/materialize.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        
        <script src="materialize/js/materialize.js" type="text/javascript"></script>
        <title>solicitud</title>
    </head>
    <body>
       <div class="container">
            <div class="row background cyan darken-3  ">

                <div class="col s6"> 
                    <div class="card ">
                        <div class="card-image"> 
                            <img src="imagenes/saluda.jpg" alt=""/>
                        </div>
                    </div>
                </div>
                <%--AQUI COMIENZA EL FORMULARIO DE INGRESO DEL NUEVO ADMINISTRADOR
                --%>

                <div class="col s6">
                    <form action="solicitud?action=insertar" method="POST" class="col s12">

                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>                          
                            <input value="" name="id_alumno" class="autocomplete" id="autocomplete-content" type="text">
                            <label for="autocomplete-content">id_alumno</label>
                        </div>

                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix"> <img src="imagenes/iconx2-000000.png" alt=""/></i>
                            <input name="id_libro" class="autocomplete" id="autocomplete-content" type="text">
                            <label for="autocomplete-content">id_libro</label>
                        </div>

                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix red-text"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>
                            <input name="fecha_solicitud" class="autocomplete" id="autocomplete-content" type="text" >
                            <label for="autocomplete-content">fecha_solicitud</label>
                        </div>
                                                                                             
                        <button class="waves-effect wabes-yellow btn ">GUARDAR</button>
                        <a href="solicitud?action=consultarAll" class="btn btn-blue">TABLA</a>                      
                        <a href="index.jsp" class="btn btn-blue">inicio</a>                      
                    </form>
                    ${msg}
                </div>
            </div>


            <script>
                document.addEventListener('DOMContentLoaded', function () {
                    M.AutoInit();

                });

            </script>
        </div>
    </body>
</html>
