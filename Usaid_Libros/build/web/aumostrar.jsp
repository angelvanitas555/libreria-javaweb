<%-- 
    Document   : pmostrar
    Created on : 01-29-2019, 09:41:16 AM
    Author     : juan.serranoUSAM
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="row navi">
            <ul>
                <li><a href="index.jsp">inicio</a></li>
                <li><a>proveedores</a>
                    <ul>
                        <li><a href="pinsertar.jsp">insertar</a></li>
                        <li><a href="proveedores?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>autores</a>
                    <ul>
                        <li><a href="auinsertar.jsp">insertar</a></li>
                        <li><a href="autores?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>alumnos</a>
                    <ul>
                        <li><a href="alinsertar.jsp">insertar</a></li>
                        <li><a href="alumnos?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>libros</a>
                    <ul>
                        <li><a href="libros?action=consultarIn">insertar</a></li>
                        <li><a href="libros?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>stock</a>
                    <ul>
                        <li><a href="stock?action=consultarIn">insertar</a></li>
                        <li><a href="stock?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>registros</a>
                    <ul>
                        <li><a href="registros?action=consultarIn">insertar</a></li>
                        <li><a href="registros?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="row title">
            TABLA DE AUTORES</div>
        <div class="container">

            <br>
            <!-- contenido -->
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped" style="background-color: #5cb3fd;color:black;">
                        <thead>
                            <tr>
                                <td>id</td>
                                <td>nombre</td>
                                <td>nacionalidad</td>
                                <td>fecha nacimiento</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="ver">
                                <tr>
                                    <td>${ver.id}</td>
                                    <td>${ver.nombre}</td>
                                    <td>${ver.nacionalidad}</td>
                                    <td>${ver.f_nac}</td>
                                    <td>
                                        <a href="autores?action=consultarById&id=${ver.id}" class="btn btn-black">modificar</a>
                                        <a href="autores?action=eliminar&id=${ver.id}" class="btn btn-blue" >eliminar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <label style="color: red;">${msg}</label><label style="color: red;">${msg1}</label>
                    <hr>
                    <a href="auinsertar.jsp" class="btn btn-black">regresar</a>
                </div>
            </div>
        </div>
    </body>
</html>
