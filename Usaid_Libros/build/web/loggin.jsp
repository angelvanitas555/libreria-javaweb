<%-- 
    Document   : loggin
    Created on : 02-04-2019, 12:13:08 AM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="materialize/css/materialize.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <script src="materialize/js/materialize.js" type="text/javascript"></script>
        <title>Loggin</title>
    </head>
    <body>
        <div class="container">
            <div class="row background cyan darken-3  ">

                <div class="col s6"> 
                    <div class="card ">
                        <div class="card-image"> 
                            <img src="imagenes/saluda.jpg" alt=""/>
                        </div>
                    </div>
                </div>
                <%--AQUI COMIENZA EL FORMULARIO DE INGRESO DEL NUEVO USUARIO
                --%>

                <div class="col s6">
                    <form action="loggin_admin?action=Log_dos" method="POST" class="col s12">
                        <label for="" class="text-darken-4">INGRESE SU USUARIO ADMINISTRADOR</label>

                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix"> <img src="icos/iconx2-000000.png" alt=""/> </i>
                            <input value="" name="usuario" class="autocomplete" id="autocomplete-content" type="text">
                            <label for="autocomplete-content">Nombre</label>
                        </div>

                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix"> <img src="icos/iconx2-000000.png" alt=""/></i>
                            <input name="pass" class="autocomplete" id="autocomplete-content" type="password">
                            <label for="autocomplete-content">Password</label>
                        </div>                       
                            
                        <button class="waves-effect wabes-yellow btn ">INGRESAR</button>
                                 <label class="text-darken-4">INGRESE SU USUARIO ADMINISTRADOR</label>             
                    </form>
                   
                </div>
            </div>

 ${msg}
            <script>
                document.addEventListener('DOMContentLoaded', function () {
                    M.AutoInit();

                });

            </script>
        </div>
    </body>
</html>
