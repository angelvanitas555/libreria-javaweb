<%-- 
    Document   : msoli
    Created on : 02-07-2019, 09:08:56 AM
    Author     : jose.azucenaUSAM
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="materialize/css/materialize.css" rel="stylesheet" type="text/css"/>
        <script src="materialize/js/materialize.js" type="text/javascript"></script>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <title>tabla solicitud</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="cols8">
                    <table class="table table-bordered bg-cyan">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>ID_ALUMNO</td>
                                <td>ID_LIBRO</td>
                                <td>FECHA</td>
                                <td>ACCIONES</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="ver">
                            <tr>
                                <td>${ver.id}</td>
                                <td>${ver.id_alumno.id}</td>
                                <td>${ver.id_libro.ISBN}</td>
                                <td>${ver.f_h_solicitud}</td>
                                <td>
                                    <a href="solicitud?action=modificar&id=${ver.id}" class="btn btn-dark">MODIFICAR</a>
                                    <a href="solicitud?action=eliminarById&id${ver.id}" class="btn btn-dark">ELIMINAR</a>
                                </td>
                            </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <a href="isoli.jsp" class="btn btn-dark">insertar</a>
                    <a href="index.jsp" class="btn btn-dark">index</a>
                </div>
            </div>
        </div>
    </body>
</html>
