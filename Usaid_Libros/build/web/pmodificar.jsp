<%-- 
    Document   : pmostrar
    Created on : 01-29-2019, 09:41:16 AM
    Author     : juan.serranoUSAM
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="row navi">
            <ul>
                <li><a href="index.jsp">inicio</a></li>
                <li><a>proveedores</a>
                    <ul>
                        <li><a href="pinsertar.jsp">insertar</a></li>
                        <li><a href="proveedores?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>autores</a>
                    <ul>
                        <li><a href="auinsertar.jsp">insertar</a></li>
                        <li><a href="autores?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>alumnos</a>
                    <ul>
                        <li><a href="alinsertar.jsp">insertar</a></li>
                        <li><a href="alumnos?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>libros</a>
                    <ul>
                        <li><a href="libros?action=consultarIn">insertar</a></li>
                        <li><a href="libros?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>stock</a>
                    <ul>
                        <li><a href="stock?action=consultarIn">insertar</a></li>
                        <li><a href="stock?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>registros</a>
                    <ul>
                        <li><a href="registros?action=consultarIn">insertar</a></li>
                        <li><a href="registros?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="row title">
            MODIFICAR LIBRO</div>
        <div class="container">

            <br>
            <!-- contenido -->
            <div class="row">
                <div class="col-12" style="font-family: cursive;">
                    <form action="proveedores?action=modificar" method="POST">
                        <c:forEach items="${lista}" var="ver">
                            <div class="row form-group">
                                <div class="col-12 input-group flex-nowrap">
                                    <input type="text" class="form-control" value="${ver.id}" name="id" placeholder="id" readonly>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-12 input-group flex-nowrap">
                                    <input type="text" class="form-control" value="${ver.nombre}" name="nombre" placeholder="nombre" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-12 input-group flex-nowrap">
                                    <input type="text" class="form-control" value="${ver.tipo}" name="tipo" placeholder="tipo" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-12 input-group flex-nowrap">
                                    <input type="text" class="form-control" value="${ver.rubro}" name="rubro" placeholder="rubro" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-12 input-group flex-nowrap">
                                    <input type="text" class="form-control" value="${ver.responsable}" name="responsable" placeholder="responsable" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-12 input-group flex-nowrap">
                                    <input type="text" class="form-control"  pattern="[0-9]{7}"  value="${ver.telefono}" name="telefono" placeholder="telefono" required>
                                </div>
                            </div>
                        </c:forEach>
                        <div class="row">
                            <div class="col-8">
                                <button class="btn btn-blue">modificar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
