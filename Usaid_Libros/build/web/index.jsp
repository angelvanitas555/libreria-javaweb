<%-- 
    Document   : index
    Created on : 01-29-2019, 08:14:06 AM
    Author     : juan.serranoUSAM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin control</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <link href="materialize/css/materialize.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="row">
            <div class="cols12">
            </div>
            <nav>
            <ul class=""><%-- UL QUE CONTIENE TODO --%>
                <li><a href="index.jsp">inicio</a></li>

                <li><a>proveedores</a>  <%-- LINK PROVEEDORES CERRADO--%>
                    <ul>
                        <li><a href="pinsertar.jsp">insertar</a></li>
                        <li><a href="proveedores?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>autores</a><%-- LINK AUTORES CERRADO--%>
                    <ul>
                        <li><a href="auinsertar.jsp">insertar</a></li>
                        <li><a href="autores?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>alumnos</a><%-- LINK ALUMNOS CERRADO--%>
                    <ul>
                        <li><a href="alumnos?action=consultarIn">insertar</a></li>
                        <li><a href="alumnos?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>libros</a><%-- LINK LIBROS CERRADO--%>
                    <ul>
                        <li><a href="libros?action=consultarIn">insertar</a></li>
                        <li><a href="libros?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>stock</a><%-- LINK STOCK CERRADO--%>
                    <ul>
                        <li><a href="stock?action=consultarIn">insertar</a></li>
                        <li><a href="stock?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>registros</a><%-- LINK REGISTROS CERRADO--%>
                    <ul>
                        <li><a href="registros?action=consultarIn">insertar</a></li>
                        <li><a href="registros?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>USUARIOS</a><%-- LINK REGISTROS CERRADO--%>
                    <ul>
                        <li><a href="usuarios?action=insertar" class="btn-danger">insertar</a></li>
                        <li><a href="usuarios?action=consultarAll" class="btn-blue">consultar</a></li>
                    </ul>
                </li>
                <li><a>Loggin</a><%-- LINK REGISTROS CERRADO--%>
                    <ul>
                        <li><a href="loggin.jsp" class="btn-danger">LOGGIN</a></li>

                    </ul>
                </li>
                <li><a>ADMINISTRADORES</a><%-- LINK REGISTROS CERRADO--%>
                    <ul>
                        <li><a href="registro_administrador.jsp" class="btn-danger">insertar</a></li>
                        <li><a href="administradores?action=consultarAll" class="btn-blue">consultar</a></li>
                    </ul>
                </li>
                <li><a>SOLICITUDES</a><%-- LINK REGISTROS CERRADO--%>
                    <ul>
                        <li><a href="isoli.jsp" class="btn-danger">insertar</a></li>
                        <li><a href="msoli.jsp" class="btn-blue">consultar</a></li>
                    </ul>
                </li>
            </ul>
            </nav>
        </div>
        <div class="row ">
            <div class="cols12">
                <div class="alert-info invitado center year-text"><h1 class="center">${msg}</h1></div>
            </div>
        </div>
    </body>
</html>
