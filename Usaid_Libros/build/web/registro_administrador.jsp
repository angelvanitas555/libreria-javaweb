<%-- 
    Document   : registro_administrador
    Created on : 02-04-2019, 12:54:53 AM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="materialize/css/materialize.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <script src="materialize/js/materialize.js" type="text/javascript"></script>
        <title>Admin registro</title>
    </head>
    <body>
         <div class="container">
            <div class="row background cyan darken-3  ">

                <div class="col s6"> 
                    <div class="card ">
                        <div class="card-image"> 
                            <img src="imagenes/saluda.jpg" alt=""/>
                        </div>
                    </div>
                </div>
                <%--AQUI COMIENZA EL FORMULARIO DE INGRESO DEL NUEVO ADMINISTRADOR
                --%>

                <div class="col s6">
                    <form action="administradores?action=insertar" method="POST" class="col s12">

                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>                          
                            <input value="" name="nombre" class="autocomplete" id="autocomplete-content" type="text">
                            <label for="autocomplete-content">nombre</label>
                        </div>

                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix"> <img src="imagenes/iconx2-000000.png" alt=""/></i>
                            <input name="apellido" class="autocomplete" id="autocomplete-content" type="text">
                            <label for="autocomplete-content">apellido</label>
                        </div>

                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix red-text"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>
                            <input name="telefono" class="autocomplete" id="autocomplete-content" type="text" >
                            <label for="autocomplete-content">telefono</label>
                        </div>
                                                  
                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix red-text"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>
                            <input name="direccion" class="autocomplete" id="autocomplete-content" type="text" >
                            <label for="autocomplete-content">direccion</label>
                        </div>
                                                  
                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix red-text"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>
                            <input name="correo" class="autocomplete" id="autocomplete-content" type="text" >
                            <label for="autocomplete-content">correo</label>
                        </div>
                                                  
                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix red-text"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>
                            <input name="cod_usuario" class="autocomplete" id="autocomplete-content" type="text" >
                            <label for="autocomplete-content">cod_usuario</label>
                        </div>
                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix red-text"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>
                            <input name="pass_admin" class="autocomplete" id="autocomplete-content" type="password" >
                            <label for="autocomplete-content">pass_admin</label>
                        </div>
                                                  
                        <button class="waves-effect wabes-yellow btn ">GUARDAR</button>
                        <a href="administradores?action=consultarAll" class="btn btn-blue">TABLA</a>                      
                        <a href="index.jsp" class="btn btn-blue">inicio</a>                      
                    </form>
                    ${msg}
                </div>
            </div>


            <script>
                document.addEventListener('DOMContentLoaded', function () {
                    M.AutoInit();

                });

            </script>
        </div>
    </body>
</html>
