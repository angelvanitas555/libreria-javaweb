<%-- 
    Document   : admin_table
    Created on : 02-04-2019, 10:53:12 AM
    Author     : jose.azucenaUSAM
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="materialize/css/materialize.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <script src="materialize/js/materialize.js" type="text/javascript"></script>
        <title>ADMIN TABLA</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="cols8">
                    <table class="table table-bordered table-warning">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>NOMBRE</td>
                                <td>APELLIDO</td>
                                <td>TELEFONO</td>
                                <td>DIRECCION</td>
                                <td>CORREO</td>
                                <td>COD DE USUARIO</td>
                                <td>PASE DE ADMINISTRADOR</td>
                                <td>ACCIONES</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="ver">
                            <tr>
                                <td>${ver.id}</td>
                                <td>${ver.nombre}</td>
                                <td>${ver.apellido}</td>
                                <td>${ver.telefono}</td>
                                <td>${ver.direccion}</td>
                                <td>${ver.correo}</td>
                                <td>${ver.cod_usuario.id}</td>
                                <td>${ver.pass_admin}</td>
                                <td>
                                    <a href="administradores?action=eliminar&id=${ver.id}" class="btn btn-blue">ELIMINAR</a>
                                    <a href="administradores?action=consultarById" class="btn-warning">MODIFICAR</a>
                                </td>
                            </tr>
                            </c:forEach>
                        </tbody>${msg}
                    </table> <a href="registro_administrador.jsp" class=" btn btn-black ">registrar</a>
                </div>
            </div>
        </div>
    </body>
</html>
