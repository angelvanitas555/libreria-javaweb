package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Admin control</title>\n");
      out.write("        <link href=\"css/bootstrap.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("        <link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        \n");
      out.write("            <ul>");
      out.write("\n");
      out.write("            <li><a href=\"index.jsp\">inicio</a></li>\n");
      out.write("            \n");
      out.write("            <li><a>proveedores</a>  ");
      out.write("\n");
      out.write("                <ul>\n");
      out.write("                    <li><a href=\"pinsertar.jsp\">insertar</a></li>\n");
      out.write("                    <li><a href=\"proveedores?action=consultarAll\">consultar</a></li>\n");
      out.write("                </ul>\n");
      out.write("            </li>\n");
      out.write("            <li><a>autores</a>");
      out.write("\n");
      out.write("                <ul>\n");
      out.write("                    <li><a href=\"auinsertar.jsp\">insertar</a></li>\n");
      out.write("                    <li><a href=\"autores?action=consultarAll\">consultar</a></li>\n");
      out.write("                </ul>\n");
      out.write("            </li>\n");
      out.write("            <li><a>alumnos</a>");
      out.write("\n");
      out.write("                <ul>\n");
      out.write("                    <li><a href=\"alumnos?action=consultarIn\">insertar</a></li>\n");
      out.write("                    <li><a href=\"alumnos?action=consultarAll\">consultar</a></li>\n");
      out.write("                </ul>\n");
      out.write("            </li>\n");
      out.write("            <li><a>libros</a>");
      out.write("\n");
      out.write("                <ul>\n");
      out.write("                    <li><a href=\"libros?action=consultarIn\">insertar</a></li>\n");
      out.write("                    <li><a href=\"libros?action=consultarAll\">consultar</a></li>\n");
      out.write("                </ul>\n");
      out.write("            </li>\n");
      out.write("            <li><a>stock</a>");
      out.write("\n");
      out.write("                <ul>\n");
      out.write("                    <li><a href=\"stock?action=consultarIn\">insertar</a></li>\n");
      out.write("                    <li><a href=\"stock?action=consultarAll\">consultar</a></li>\n");
      out.write("                </ul>\n");
      out.write("            </li>\n");
      out.write("            <li><a>registros</a>");
      out.write("\n");
      out.write("                <ul>\n");
      out.write("                    <li><a href=\"registros?action=consultarIn\">insertar</a></li>\n");
      out.write("                    <li><a href=\"registros?action=consultarAll\">consultar</a></li>\n");
      out.write("                </ul>\n");
      out.write("            </li>\n");
      out.write("            <li><a>USUARIOS</a>");
      out.write("\n");
      out.write("                <ul>\n");
      out.write("                    <li><a href=\"usuarios?action=insertar\" class=\"btn-danger\">insertar</a></li>\n");
      out.write("                    <li><a href=\"usuarios?action=consultarAll\" class=\"btn-blue\">consultar</a></li>\n");
      out.write("                </ul>\n");
      out.write("            </li>\n");
      out.write("            <li><a>Loggin</a>");
      out.write("\n");
      out.write("                <ul>\n");
      out.write("                    <li><a href=\"loggin.jsp\" class=\"btn-danger\">LOGGIN</a></li>\n");
      out.write("                    \n");
      out.write("                </ul>\n");
      out.write("            </li>\n");
      out.write("            <li><a>ADMINISTRADORES</a>");
      out.write("\n");
      out.write("                <ul>\n");
      out.write("                    <li><a href=\"registro_administrador.jsp\" class=\"btn-danger\">insertar</a></li>\n");
      out.write("                    <li><a href=\"administradores?action=consultarAll\" class=\"btn-blue\">consultar</a></li>\n");
      out.write("                </ul>\n");
      out.write("            </li>\n");
      out.write("        </ul>\n");
      out.write("                <div class=\"alert-info\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${msg}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
