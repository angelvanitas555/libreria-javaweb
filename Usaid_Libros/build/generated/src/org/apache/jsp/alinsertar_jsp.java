package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class alinsertar_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("        <link href=\"css/bootstrap.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("        <link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"row navi\">\n");
      out.write("            <ul>\n");
      out.write("                <li><a href=\"index.jsp\">inicio</a></li>\n");
      out.write("                <li><a>proveedores</a>\n");
      out.write("                    <ul>\n");
      out.write("                        <li><a href=\"pinsertar.jsp\">insertar</a></li>\n");
      out.write("                        <li><a href=\"proveedores?action=consultarAll\">consultar</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </li>\n");
      out.write("                <li><a>autores</a>\n");
      out.write("                    <ul>\n");
      out.write("                        <li><a href=\"auinsertar.jsp\">insertar</a></li>\n");
      out.write("                        <li><a href=\"autores?action=consultarAll\">consultar</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </li>\n");
      out.write("                <li><a>alumnos</a>\n");
      out.write("                    <ul>\n");
      out.write("                        <li><a href=\"alinsertar.jsp\">insertar</a></li>\n");
      out.write("                        <li><a href=\"alumnos?action=consultarAll\">consultar</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </li>\n");
      out.write("                <li><a>libros</a>\n");
      out.write("                    <ul>\n");
      out.write("                        <li><a href=\"libros?action=consultarIn\">insertar</a></li>\n");
      out.write("                        <li><a href=\"libros?action=consultarAll\">consultar</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </li>\n");
      out.write("                <li><a>stock</a>\n");
      out.write("                    <ul>\n");
      out.write("                        <li><a href=\"stock?action=consultarIn\">insertar</a></li>\n");
      out.write("                        <li><a href=\"stock?action=consultarAll\">consultar</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </li>\n");
      out.write("                <li><a>registros</a>\n");
      out.write("                    <ul>\n");
      out.write("                        <li><a href=\"registros?action=consultarIn\">insertar</a></li>\n");
      out.write("                        <li><a href=\"registros?action=consultarAll\">consultar</a></li>\n");
      out.write("                    </ul>\n");
      out.write("                </li>\n");
      out.write("            </ul>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"row title\">\n");
      out.write("            INSERTAR ALUMNO</div>\n");
      out.write("        <div class=\"container\">\n");
      out.write("\n");
      out.write("            <br>\n");
      out.write("            <!-- contenido -->\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"col-12\" style=\"font-family: cursive;\">\n");
      out.write("                    <form action=\"alumnos?action=insertar\" method=\"POST\">\n");
      out.write("                        <div class=\"row form-group\">\n");
      out.write("                            <div class=\"input-group flex-nowrap\">\n");
      out.write("                                <input type=\"text\" class=\"form-control\" name=\"nombre\" placeholder=\"nombre\" required>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"row form-group\">\n");
      out.write("                            <div class=\"col-12 input-group flex-nowrap\">\n");
      out.write("                                <input type=\"text\" class=\"form-control\" name=\"carrera\" placeholder=\"carrera\" required>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"row form-group\">\n");
      out.write("                            <div class=\"col-12 input-group flex-nowrap\"> ");
      out.write("\n");
      out.write("                                <!-- <input type=\"text\" class=\"form-control\" name=\"cod_usuario\" placeholder=\"cod_usuario\" required> -->\n");
      out.write("                                <select class=\"custom-select green\" value=\"\" name=\"cod_usuario\">\n");
      out.write("                                    <option  class=\"disabled\">usurarios</option>\n");
      out.write("                                    ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                                </select> \n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-8 form-group\">\n");
      out.write("                                <button class=\"btn btn-black\">guardar</button>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <div class=\"row form-group\">\n");
      out.write("                            <div class=\"col-8\">\n");
      out.write("                                <a href=\"alumnos?action=consultarAll\" class=\"btn btn-blue\">mostrar</a>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </form>\n");
      out.write("                    <label style=\"color: #1b6d85;\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${msg}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</label>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${lista}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("ver");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                                     <option  value=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${ver.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\">soy un vacio</option>\n");
          out.write("                                    ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }
}
