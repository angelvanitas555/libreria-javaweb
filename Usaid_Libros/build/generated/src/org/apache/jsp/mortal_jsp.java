package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class mortal_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link href=\"materialize/css/materialize.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("        <script src=\"materialize/js/materialize.js\" type=\"text/javascript\"></script>\n");
      out.write("        <link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("        <title>REGISTRATE</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("         <div class=\"container\">\n");
      out.write("            <div class=\"row background cyan darken-3  \">\n");
      out.write("\n");
      out.write("                <div class=\"col s6\"> \n");
      out.write("                    <div class=\"card \">\n");
      out.write("                        <div class=\"card-image\"> \n");
      out.write("                            <img src=\"imagenes/saluda.jpg\" alt=\"\"/>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                ");
      out.write("\n");
      out.write("\n");
      out.write("                <div class=\"col s6\">\n");
      out.write("                    <form action=\"usuarios?action=insertar\" method=\"POST\" class=\"col s12\">\n");
      out.write("\n");
      out.write("                        <div class=\"input-field\">\n");
      out.write("                            <i class=\"mdi-action-account-circle prefix\"> <img src=\"imagenes/iconx2-000000.png\" alt=\"\"/> </i>                          \n");
      out.write("                            <input value=\"\" name=\"usuario\" class=\"autocomplete\" id=\"autocomplete-content\" type=\"text\">\n");
      out.write("                            <label for=\"autocomplete-content\">USUARIO</label>\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                        <div class=\"input-field\">\n");
      out.write("                            <i class=\"mdi-action-account-circle prefix\"> <img src=\"imagenes/iconx2-000000.png\" alt=\"\"/></i>\n");
      out.write("                            <input name=\"pass\" class=\"autocomplete\" id=\"autocomplete-content\" type=\"text\">\n");
      out.write("                            <label for=\"autocomplete-content\">CONTRSEÑA</label>\n");
      out.write("                        </div>                       \n");
      out.write("                                                  \n");
      out.write("                        <button class=\"waves-effect wabes-yellow btn \">GUARDAR</button>\n");
      out.write("                        <a href=\"mortal?action=insertar_mortal\" class=\"btn btn-blue\">TABLA</a>                      \n");
      out.write("                    </form>\n");
      out.write("                    ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${msg}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\n");
      out.write("                    <a href=\"usuario_comun.jsp\" class=\"btn btn-blue\">Inicio Usuario</a>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("            <script>\n");
      out.write("                document.addEventListener('DOMContentLoaded', function () {\n");
      out.write("                    M.AutoInit();\n");
      out.write("\n");
      out.write("                });\n");
      out.write("\n");
      out.write("            </script>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
