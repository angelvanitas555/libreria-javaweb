<%-- 
    Document   : uinsertar
    Created on : 02-03-2019, 01:45:01 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="materialize/css/materialize.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <title>INSERTAR USUARIO</title>
    </head>
    <body>
      <div class="container">
            <div class="row background cyan darken-3  ">

                <div class="col s6"> 
                    <div class="card ">
                        <div class="card-image"> 
                            <img src="imagenes/saluda.jpg" alt=""/>
                        </div>
                    </div>
                </div>
                <%--AQUI COMIENZA EL FORMULARIO DE INGRESO DEL NUEVO USUARIO
                --%>

                <div class="col s6">
                    <form action="usuarios?action=insertar" method="POST" class="col s12">

                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>                          
                            <input value="" name="usuario" class="autocomplete" id="autocomplete-content" type="text">
                            <label for="autocomplete-content">USUARIO</label>
                        </div>

                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix"> <img src="imagenes/iconx2-000000.png" alt=""/></i>
                            <input name="pass" class="autocomplete" id="autocomplete-content" type="text">
                            <label for="autocomplete-content">CONTRSEÑA</label>
                        </div>

                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix red-text"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>
                            <input name="tipo" class="autocomplete" id="autocomplete-content" type="text" >
                            <label for="autocomplete-content">TIPO</label>
                        </div>
                                                  
                        <button class="waves-effect wabes-yellow btn ">GUARDAR</button>
                        <a href="usuarios?action=consultarAll" class="btn btn-blue">TABLA</a>                      
                    </form>
                    ${msg}
                </div>
            </div>


            <script>
                document.addEventListener('DOMContentLoaded', function () {
                    M.AutoInit();

                });

            </script>
        </div>
    </body>
</html>
