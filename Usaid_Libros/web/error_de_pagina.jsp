<%-- 
    Document   : error_de_pagina
    Created on : 02-04-2019, 02:17:30 PM
    Author     : jose.azucenaUSAM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="materialize/css/materialize.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <script src="materialize/js/materialize.js" type="text/javascript"></script>
        <title>ERROR DE INGRESO</title>
    </head>
    <body class="background img-fluid">
        <h1 class="center">ERROR DE INGRESO</h1>
        <div class="container bg-dark">
            <div class="row bg-cyan">
                <div class="cols4">
                    <div class="card text-accent-4 tex-green center rotulo">HAS INGRESDADO UN DATO ERRONEO EN EL FORMULARIO DE INGRESO</div>
                    <a href="loggin.jsp" class="btn btn-dark">LOGGIN</a>
                </div>
            </div>
        </div>
    </body>
</html>
