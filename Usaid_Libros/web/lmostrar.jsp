<%-- 
    Document   : pmostrar
    Created on : 01-29-2019, 09:41:16 AM
    Author     : juan.serranoUSAM
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="row navi">
            <ul>
                <li><a href="index.jsp">inicio</a></li>
                <li><a>proveedores</a>
                    <ul>
                        <li><a href="pinsertar.jsp">insertar</a></li>
                        <li><a href="proveedores?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>autores</a>
                    <ul>
                        <li><a href="auinsertar.jsp">insertar</a></li>
                        <li><a href="autores?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>alumnos</a>
                    <ul>
                        <li><a href="alinsertar.jsp">insertar</a></li>
                        <li><a href="alumnos?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>libros</a>
                    <ul>
                        <li><a href="libros?action=consultarIn">insertar</a></li>
                        <li><a href="libros?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>stock</a>
                    <ul>
                        <li><a href="stock?action=consultarIn">insertar</a></li>
                        <li><a href="stock?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>registros</a>
                    <ul>
                        <li><a href="registros?action=consultarIn">insertar</a></li>
                        <li><a href="registros?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="row title">
            TABLA DE LIBROS</div>
        <div class="container">

            <br>
            <!-- contenido -->
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped" style="background-color: #5cb3fd;color:black;">
                        <thead>
                            <tr>
                                <td>ISBN</td>
                                <td>titulo</td>
                                <td>f_publicacion</td>
                                <td>n_tomo</td>
                                <td>n_edicion</td>
                                <td>genero</td>
                                <td>n_pag</td>
                                <td>editorial</td>
                                <td>autor</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="ver">
                                <tr>
                                    <td>${ver.ISBN}</td>
                                    <td>${ver.titulo}</td>
                                    <td>${ver.f_publicacion}</td>
                                    <td>${ver.n_tomo}</td>
                                    <td>${ver.n_edicion}</td>
                                    <td>${ver.genero}</td>
                                    <td>${ver.n_pag}</td>
                                    <td>${ver.editorial}</td>
                                    <td>${ver.autor.nombre}</td>
                                    <td>
                                        <a href="libros?action=consultarById&ISBN=${ver.ISBN}" class="btn btn-black">modificar</a>
                                        <a href="libros?action=eliminar&ISBN=${ver.ISBN}" class="btn btn-blue">eliminar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <label style="color: #398439;">${msg}</label><label style="color: red;">${msg1}</label>
                    <hr>
                    <a href="linsertar.jsp" class="btn btn-black">regresar</a>
                </div>
            </div>
        </div>
    </body>
</html>
