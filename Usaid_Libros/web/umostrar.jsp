<%-- 
    Document   : umostrar
    Created on : 02-03-2019, 02:59:37 PM
    Author     : user
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="materialize-v1.0.0/materialize/js/materialize.js" type="text/javascript"></script>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <title>Tabla Usuario</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <table class="table table-bordered table-dark bg-primary">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>USUARIO</td>
                                <td>PASSWORD</td>
                                <td>TIPO</td>
                                <td>ACCIONES</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="ver">
                                <tr>
                                    <td>${ver.id}</td>
                                    <td>${ver.usuario}</td>
                                    <td>${ver.pass}</td>
                                    <td>${ver.tipo}</td>
                                    <td>
                                        <a href="usuarios?action=consultarById&id=${ver.id}" class="btn btn-success">MODIFICAR</a>
                                        <a href="usuarios?action=eliminar&id=${ver.id}" class="btn btn-danger">ELIMINAR</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>${msg}
                    <a href="index.jsp" class="btn bg-success">inicio</a>
                </div>
            </div>
        </div>
                <script>
                document.addEventListener('DOMContentLoaded', function () {
                    M.AutoInit();
                });
            </script>
    </body>
</html>
