<%-- 
    Document   : umodificar
    Created on : 02-03-2019, 02:24:08 PM
    Author     : user
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="materialize/css/materialize.css" rel="stylesheet" type="text/css"/>
        <script src="materialize/js/materialize.js" type="text/javascript"></script>
        <title>Modificar Usuario</title>
    </head>
    <body>
        <div class="container">
            <div class="row background cyan darken-3  ">

                <div class="col s6"> 
                    <div class="card ">
                        <div class="card-image"> 
                            <img src="imagenes/saluda.jpg" alt=""/>
                        </div>
                    </div>
                </div>
                <%--AQUI COMIENZA EL FORMULARIO DE INGRESO DEL NUEVO USUARIO
                --%>

                <div class="col s6">
                    <form action="usuarios?action=modificar" method="POST" class="col s12">
                        <c:forEach items="${lista}" var="ver">

                            <div class="input-field">
                                <i class="mdi-action-account-circle prefix"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>                          
                                <input value="${ver.id}" name="id" class="form-control" id="autocomplete-content" type="text">
                                <label for="autocomplete-content">USUARIO</label>
                            </div>


                            <div class="input-field">
                                <i class="mdi-action-account-circle prefix"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>                          
                                <input value="${ver.usuario}" name="usuario" class="form-control" id="autocomplete-content" type="text">
                                <label for="autocomplete-content">USUARIO</label>
                            </div>

                            <div class="input-field">
                                <i class="mdi-action-account-circle prefix"> <img src="imagenes/iconx2-000000.png" alt=""/></i>
                                <input value="${ver.pass}" name="pass" class="form-control" id="autocomplete-content" type="text">
                                <label for="autocomplete-content">CONTRSEÑA</label>
                            </div>

                            <div class="input-field">
                                <i class="mdi-action-account-circle prefix red-text"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>
                                <input value="${ver.tipo}" name="tipo" class="form-control" id="autocomplete-content" type="text" >
                                <label for="autocomplete-content">TIPO</label>
                            </div>

                            <button class="waves-effect wabes-yellow btn ">GUARDAR</button>
                        </c:forEach>    hola            
                    </form>
                 
                </div>
            </div>


            <script>
                document.addEventListener('DOMContentLoaded', function () {
                    M.AutoInit();

                });

            </script>
        </div>
    </body>
</html>
