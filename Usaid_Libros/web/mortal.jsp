<%-- 
    Document   : mortal
    Created on : 02-07-2019, 01:58:15 PM
    Author     : jose.azucenaUSAM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="materialize/css/materialize.css" rel="stylesheet" type="text/css"/>
        <script src="materialize/js/materialize.js" type="text/javascript"></script>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <title>REGISTRATE</title>
    </head>
    <body>
         <div class="container">
            <div class="row background cyan darken-3  ">

                <div class="col s6"> 
                    <div class="card ">
                        <div class="card-image"> 
                            <img src="imagenes/saluda.jpg" alt=""/>
                        </div>
                    </div>
                </div>
                <%--AQUI COMIENZA EL FORMULARIO DE INGRESO DEL NUEVO USUARIO
                --%>

                <div class="col s6">
                    <form action="usuarios?action=insertar" method="POST" class="col s12">

                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix"> <img src="imagenes/iconx2-000000.png" alt=""/> </i>                          
                            <input value="" name="usuario" class="autocomplete" id="autocomplete-content" type="text">
                            <label for="autocomplete-content">USUARIO</label>
                        </div>

                        <div class="input-field">
                            <i class="mdi-action-account-circle prefix"> <img src="imagenes/iconx2-000000.png" alt=""/></i>
                            <input name="pass" class="autocomplete" id="autocomplete-content" type="text">
                            <label for="autocomplete-content">CONTRSEÑA</label>
                        </div>                       
                                                  
                        <button class="waves-effect wabes-yellow btn ">GUARDAR</button>
                        <a href="usuario_comun.jsp" class="btn btn-blue">Inicio Usuario</a>
                                           
                    </form>
                    ${msg}
                    
                </div>
            </div>


            <script>
                document.addEventListener('DOMContentLoaded', function () {
                    M.AutoInit();

                });

            </script>
        </div>
    </body>
</html>
