<%-- 
    Document   : pmostrar
    Created on : 01-29-2019, 09:41:16 AM
    Author     : juan.serranoUSAM
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="row navi" >
            <ul>
                <li><a href="index.jsp">inicio</a></li>
                <li><a>proveedores</a>
                    <ul>
                        <li><a href="pinsertar.jsp">insertar</a></li>
                        <li><a href="proveedores?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>autores</a>
                    <ul>
                        <li><a href="auinsertar.jsp">insertar</a></li>
                        <li><a href="autores?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>alumnos</a>
                    <ul>
                        <li><a href="alinsertar.jsp">insertar</a></li>
                        <li><a href="alumnos?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>libros</a>
                    <ul>
                        <li><a href="libros?action=consultarIn">insertar</a></li>
                        <li><a href="libros?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>stock</a>
                    <ul>
                        <li><a href="stock?action=consultarIn">insertar</a></li>
                        <li><a href="stock?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
                <li><a>registros</a>
                    <ul>
                        <li><a href="registros?action=consultarIn">insertar</a></li>
                        <li><a href="registros?action=consultarAll">consultar</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="row title">
            INSERTAR LIBRO</div>
        <div class="container">

            <br>
            <!-- contenido -->
            <div class="row">
                <div class="col-12" style="font-family: cursive;">
                    <form action="libros?action=insertar" method="POST">
                        <div class="row form-group">
                            <div class="input-group flex-nowrap">
                                <input type="text" class="form-control " name="titulo" placeholder="titulo" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12 input-group flex-nowrap">
                                <input type="text" class="form-control " name="f_publicacion" placeholder="f_publicacion" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="input-group flex-nowrap">
                                <input type="text" class="form-control " name="n_tomo" placeholder="n_tomo" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12 input-group flex-nowrap">
                                <input type="text" class="form-control " name="n_edicion" placeholder="n_edicion" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="input-group flex-nowrap">
                                <input type="text" class="form-control " name="genero" placeholder="genero" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12 input-group flex-nowrap">
                                <input type="text" class="form-control " name="n_pag" placeholder="n_pag" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="input-group flex-nowrap">
                                <input type="text" class="form-control " name="editorial" placeholder="editorial" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12 input-group flex-nowrap">
                                <select class="custom-select green" value="" name="autor">
                                    <option  class="disabled">Autor</option>
                                    <c:forEach items="${lista}" var="ver">
                                        <option  value="${ver.id}">${ver.nombre}</option>
                                    </c:forEach>
                                </select> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8 form-group">
                                <button class="btn btn-black">guardar</button>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-8">
                                <a href="libros?action=consultarAll" class="btn btn-blue">mostrar</a>
                            </div>
                        </div>
                    </form>
                    <label style="color: #1b6d85;">${msg}</label>
                </div>
            </div>
        </div>
    </body>
</html>
