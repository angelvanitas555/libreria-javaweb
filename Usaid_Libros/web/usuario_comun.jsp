<%-- 
    Document   : usuario_comun.jsp
    Created on : 02-05-2019, 08:13:03 AM
    Author     : jose.azucenaUSAM
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="materialize/css/materialize.css" rel="stylesheet" type="text/css"/>
        <script src="materialize/js/materialize.js" type="text/javascript"></script>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <title> Usuario</title>
    </head>
    <body>
        <div class="row">
            <div class="cols12">
        </div>
        <ul><%-- UL QUE CONTIENE TODO --%>
            <li><a href="index.jsp">inicio</a></li>
            
            
            <li><a>autores</a><%-- LINK AUTORES CERRADO--%>
                <ul>                    
                    <li><a href="autores?action=consultarAll">consultar</a></li>
                </ul>
            </li>           
            <li><a>libros</a><%-- LINK LIBROS CERRADO--%>
                <ul>                    
                    <li><a href="libros?action=consultarAll">consultar</a></li>
                </ul>
            </li>
            <li><a>stock</a><%-- LINK STOCK CERRADO--%>
                <ul>                                     
                    <li><a href="stock?action=consultarAll">consultar</a></li>
                </ul>
            </li>            
            <li><a>USUARIOS</a><%-- LINK REGISTROS CERRADO--%>
                <ul>
                    <li><a href="mortal.jsp" class="btn-danger">Registrate</a></li>
                    <li><a href="usuarios?action=consultarAll" class="btn-blue">consultar</a></li>
                </ul>
            </li>
            <li><a>Loggin</a><%-- LINK REGISTROS CERRADO--%>
                <ul>
                    <li><a href="loggin.jsp" class="btn-danger">LOGGIN</a></li>                   
                </ul>
            </li>           
        </ul>
                </div>
                <div class="row ">
                    <div class="cols12">
                        <div class="alert-info invitado center year-text"><h1>${msg}</h1></div>
                    </div>
                </div>
    </body>
</html>
